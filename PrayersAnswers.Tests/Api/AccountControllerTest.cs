﻿using Microsoft.AspNet.Identity;
using NUnit.Framework;
using PrayersAnswers.Api;
using PrayersAnswers.Api.Entities;
using PrayersAnswers.Api.Models;
using PrayersAnswers.Models;
using PrayersAnswers.Tests.Fakes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace PrayersAnswers.Tests.Api
{
    [TestFixture]
    class AccountControllerTest
    {
        FakeDataRepo _repo;
        AccountController _AccountController;

        [SetUp]
        public void FixtureSetUp()
        {
            List<Prayer> Prayers = FakeData.CreateFakePrayers();
            List<Answer> Answers = FakeData.CreateFakeAnswers(Prayers);
            UserManager<ApplicationUser> UserManager = FakeData.CreateFakeUserManager();

            List<Client> Clients = FakeData.CreateFakeClients();
            List<RefreshToken> RefreshTokens = FakeData.CreateFakeRefreshTokens();
            _repo = new FakeDataRepo(Prayers, Answers, UserManager);
            _AccountController = new AccountController(_repo);
            //_PrayerController = new PrayerController(_repo);
            //_AnswerController = new AnswerController(_repo);
            
        }

        #region register

        [Test]
        public async Task Register_New_User_With_Matching_Password_returns_OK()
        {
            //arrange

            UserModel NewUserModel = new UserModel();
            NewUserModel.UserName = "abc";
            NewUserModel.Password = "abcdefg";
            NewUserModel.ConfirmPassword = "abcdefg";
            NewUserModel.EmailAddress = "abc@abc.com";

            //act
            var result = await _AccountController.Register(NewUserModel);
            var content = result as System.Web.Http.Results.OkResult;



            //assert

            //NUnit.Framework.Assert.IsNull(content);
            NUnit.Framework.Assert.IsNotNull(content);
        }

        [Test]
        public async Task Register_New_User_With_Non_Matching_Password_returns_BadReq()
        {
            //arrange

            UserModel NewUserModel = new UserModel();
            NewUserModel.UserName = "def";
            NewUserModel.Password = "abcdefg";
            NewUserModel.ConfirmPassword = "abcdefg1";
            NewUserModel.EmailAddress = "abc@abc.com";

            //act
            var result = await _AccountController.Register(NewUserModel);            
            var content = result as System.Web.Http.Results.BadRequestResult;
            

            //assert

            //NUnit.Framework.Assert.IsNull(content);
            NUnit.Framework.Assert.IsNotNull(content);
        }

        

        #endregion register
    }
}
