﻿
using Microsoft.AspNet.Identity;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using PrayersAnswers.Api;
using PrayersAnswers.Models;
using PrayersAnswers.Tests.Fakes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Routing;

namespace PrayersAnswers.Tests.Api
{
    [TestFixture]
    class AnswerControllerTest
    {
        FakeDataRepo _repo;
        AnswerController _AnswerController;

        [SetUp]
        public void FixtureSetUp()
        {
            List<Prayer> Prayers = FakeData.CreateFakePrayers();
            List<Answer> Answers = FakeData.CreateFakeAnswers(Prayers);
            UserManager<ApplicationUser> UserManager = FakeData.CreateFakeUserManager();
            _repo = new FakeDataRepo(Prayers, Answers, UserManager);
            //_PrayerController = new PrayerController(_repo);
            _AnswerController = new AnswerController(_repo);
        }

        #region new answers

        [Test]
        public async Task Add_New_Answer_With_Matching_User_adds_one_more_answer()
        {
            //arrange
            var identity = new GenericIdentity("abc");

            _AnswerController.User = new GenericPrincipal(identity, null);

            var result = _AnswerController.GetAnswersCount();
            var content = result.Result as System.Web.Http.Results.OkNegotiatedContentResult<int>;

            int CurrentPrayerCount = content.Content;


            //act
            AnswerVM NewAnswer = new AnswerVM();


            NewAnswer.UserID = "abc";
            NewAnswer.Notes = "new notes";
            NewAnswer.PrayerID = 26;
            

            var postresult = await _AnswerController.Post(NewAnswer);

            result = _AnswerController.GetAnswersCount();
            content = result.Result as System.Web.Http.Results.OkNegotiatedContentResult<int>;
            int NewCount = content.Content;

            //assert
            //NUnit.Framework.Assert.AreEqual(CurrentPrayerCount, NewCount);
            NUnit.Framework.Assert.AreEqual(CurrentPrayerCount + 1, NewCount);

        }

        [Test]
        public async Task Add_New_Answer_using_prayer_with_existing_answer_returns_BadReq()
        {
            //arrange
            var identity = new GenericIdentity("abc");

            _AnswerController.User = new GenericPrincipal(identity, null);

            var result = _AnswerController.GetAnswersCount();
            var content = result.Result as System.Web.Http.Results.OkNegotiatedContentResult<int>;

            int CurrentPrayerCount = content.Content;


            //act
            AnswerVM NewAnswer = new AnswerVM();


            NewAnswer.UserID = "abc";
            NewAnswer.Notes = "new notes";
            NewAnswer.PrayerID = 1;


            var postresult = await _AnswerController.Post(NewAnswer);

            var NewContent = postresult as System.Web.Http.Results.BadRequestResult;


            //assert
            NUnit.Framework.Assert.NotNull(NewContent);
            //NUnit.Framework.Assert.Null(NewContent);

        }

        [Test]
        public async Task Add_New_Answer_With_Prayer_not_matching_User_returns_BadReq()
        {
            //arrange
            var identity = new GenericIdentity("abc");

            _AnswerController.User = new GenericPrincipal(identity, null);

            var result = _AnswerController.GetAnswersCount();
            var content = result.Result as System.Web.Http.Results.OkNegotiatedContentResult<int>;

            int CurrentPrayerCount = content.Content;


            //act
            AnswerVM NewAnswer = new AnswerVM();


            NewAnswer.UserID = "abc";
            NewAnswer.Notes = "new notes";
            NewAnswer.PrayerID = 31;


            var postresult = await _AnswerController.Post(NewAnswer);

            var NewContent = postresult as System.Web.Http.Results.BadRequestResult;


            //assert
            NUnit.Framework.Assert.NotNull(NewContent);
            //NUnit.Framework.Assert.Null(NewContent);

        }

        [Test]
        public async Task Add_New_Answer_With_no_user_returns_BadReq()
        {
            //arrange
            var identity = new GenericIdentity("");

            _AnswerController.User = new GenericPrincipal(identity, null);
            

            //act
            AnswerVM NewAnswer = new AnswerVM();


            NewAnswer.UserID = "abc";
            NewAnswer.Notes = "new notes";
            NewAnswer.PrayerID = 1;


            var postresult = await _AnswerController.Post(NewAnswer);

            var NewContent = postresult as System.Web.Http.Results.BadRequestResult;


            //assert
            NUnit.Framework.Assert.NotNull(NewContent);
            //NUnit.Framework.Assert.Null(NewContent);

        }

        #endregion new answers


        #region get answers

        [Test]

        public void Get_Answers_With_No_User_Returns_BadRequest()
        {
            //arrange
            var identity = new GenericIdentity("");
            _AnswerController.User = new GenericPrincipal(identity, null);

            //act
            var result = _AnswerController.GetAnswersByPage();
            var content = result.Result as System.Web.Http.Results.BadRequestResult;

            //assert
            NUnit.Framework.Assert.IsNotNull(content);
            //NUnit.Framework.Assert.Null(content);


        }

        [Test]
        public void Get_Prayers_With_User_abc_returns_more_than_1()
        {
            //arrange
            var identity = new GenericIdentity("abc");
            _AnswerController.User = new GenericPrincipal(identity, null);

            //act
            var result = _AnswerController.GetAnswersCount();
            //var content = result.Result as System.Web.Http.Results.OkNegotiatedContentResult<System.Collections.Generic.List<PrayersAnswers.Models.Prayer>>;
            var content = result.Result as System.Web.Http.Results.OkNegotiatedContentResult<int>;

            //assert
            //NUnit.Framework.Assert.Null(content);
            //NUnit.Framework.Assert.Less(content.Content, 1);

            NUnit.Framework.Assert.IsNotNull(content);
            NUnit.Framework.Assert.GreaterOrEqual(content.Content, 1);
        }

        [Test]
        public async Task Get_Answers_2nd_page_with_user_abc_returns_10_with_matching_indexes()
        {
            //arrange
            var identity = new GenericIdentity("abc");
            _AnswerController.User = new GenericPrincipal(identity, null);

            //act
            var result = await _AnswerController.GetAnswersByPage(2, 10);
            var content = result as System.Web.Http.Results.OkNegotiatedContentResult<System.Collections.Generic.List<PrayersAnswers.Models.Answer>>;

            //assert
            int i = 11;
            foreach (var ThisAnswer in content.Content)
            {

                //NUnit.Framework.Assert.IsFalse(ThisAnswer.Notes.Contains(i.ToString()));
                NUnit.Framework.Assert.IsTrue(ThisAnswer.Notes.Contains(i.ToString()));
                i++;
            }
        }

        [Test]
        public async Task Get_Prayers_Without_User_returns_badrequest()
        {
            //arrange
            var identity = new GenericIdentity("");
            _AnswerController.User = new GenericPrincipal(identity, null);

            //act
            var result = await _AnswerController.GetAnswersByPage(2, 10);
            var content = result as System.Web.Http.Results.BadRequestResult;


            //assert
            //NUnit.Framework.Assert.Null(content);
            NUnit.Framework.Assert.NotNull(content);
        }

        [Test]
        public async Task Get_Prayers_With_User_abc_Without_PageNum_returns_first_10()
        {
            //arrange
            var identity = new GenericIdentity("abc");
            _AnswerController.User = new GenericPrincipal(identity, null);

            //act
            var result = await _AnswerController.GetAnswersByPage(ItemsPerPage: 10);
            var content = result as System.Web.Http.Results.OkNegotiatedContentResult<System.Collections.Generic.List<PrayersAnswers.Models.Answer>>;

            //assert
            int i = 1;
            foreach (var ThisAnswer in content.Content)
            {
                //NUnit.Framework.Assert.IsFalse(ThisAnswer.Notes.Contains(i.ToString()));
                NUnit.Framework.Assert.IsTrue(ThisAnswer.Notes.Contains(i.ToString()));
                i++;
            }
        }

        [Test]
        public async Task Get_Prayers_With_User_abc_Without_ItemsPerPage_returns_10()
        {
            //arrange
            var identity = new GenericIdentity("abc");
            _AnswerController.User = new GenericPrincipal(identity, null);

            //act
            var result = await _AnswerController.GetAnswersByPage(PageNum: 1);
            var content = result as System.Web.Http.Results.OkNegotiatedContentResult<System.Collections.Generic.List<PrayersAnswers.Models.Answer>>;

            //assert
            //NUnit.Framework.Assert.AreNotEqual(content.Content.Count, 10);
            NUnit.Framework.Assert.AreEqual(content.Content.Count, 10);

        }

        #endregion get answers


        #region update answers

        // edit without user login
        [Test]
        public async Task Update_Answer_Without_User_Returns_BadReq()
        {
            //arrange
            var identity = new GenericIdentity("");
            _AnswerController.User = new GenericPrincipal(identity, null);

            AnswerVM ItemToUpdate = new AnswerVM();
            ItemToUpdate.Notes = "modified";
            


            //act
            var result = await _AnswerController.Put(ItemToUpdate);
            var content = result as System.Web.Http.Results.BadRequestResult;


            //assert
            //NUnit.Framework.Assert.Null(content);
            NUnit.Framework.Assert.NotNull(content);
        }




        // edit with user login not matching userid
        [Test]
        public async Task Update_Answer_With_non_matching_Username_Returns_BadReq()
        {
            //arrange
            var identity = new GenericIdentity("def");
            _AnswerController.User = new GenericPrincipal(identity, null);

            AnswerVM ItemToUpdate = new AnswerVM();
            ItemToUpdate.Notes = "modified";
            ItemToUpdate.Id = 1;
            ItemToUpdate.UserID = "abc";


            //act
            var result = await _AnswerController.Put(ItemToUpdate);
            var content = result as System.Web.Http.Results.BadRequestResult;


            //assert
            //NUnit.Framework.Assert.Null(content);
            NUnit.Framework.Assert.NotNull(content);
        }

        // edit with user login, matching userid
        [Test]
        public async Task Update_Answer_With_Matching_Username_Returns_Changed_Record()
        {
            //arrange
            var identity = new GenericIdentity("abc");
            _AnswerController.User = new GenericPrincipal(identity, null);

            AnswerVM ItemToUpdate = new AnswerVM();
            ItemToUpdate.Notes = "modified1";
            ItemToUpdate.Id = 1;
            ItemToUpdate.UserID = "abc";


            //act
            var result = await _AnswerController.Put(ItemToUpdate);
            var content = result as System.Web.Http.Results.OkNegotiatedContentResult<Answer>;



            //assert
            //NUnit.Framework.Assert.AreNotEqual(content.Content.Notes, "modified1");
            NUnit.Framework.Assert.AreEqual(content.Content.Notes, "modified1");
        }

        #endregion update answers


        #region delete answers

        //test without user
        [Test]
        public async Task Delete_Answer_Without_User_Returns_BadReq()
        {
            //arrange
            var identity = new GenericIdentity("");
            _AnswerController.User = new GenericPrincipal(identity, null);

            //act
            var result = await _AnswerController.Delete(1);
            var content = result as System.Web.Http.Results.BadRequestResult;


            //assert
            //NUnit.Framework.Assert.Null(content);
            NUnit.Framework.Assert.NotNull(content);


        }

        //test with user, non-matching userId
        [Test]
        public async Task Delete_Answer_With_User_abc_non_matching_username_BadReq()
        {

            //arrange
            var identity = new GenericIdentity("abc");
            _AnswerController.User = new GenericPrincipal(identity, null);

            //act
            var result = await _AnswerController.Delete(50);
            var content = result as System.Web.Http.Results.BadRequestErrorMessageResult;

            //assert
            //NUnit.Framework.Assert.Null(content);
            NUnit.Framework.Assert.NotNull(content);
        }

        //test with user, matching userId
        [Test]
        public async Task Delete_Answer_With_User_abc_matching_username_returns_ok()
        {
            //arrange
            var identity = new GenericIdentity("abc");
            _AnswerController.User = new GenericPrincipal(identity, null);
            var countresult = await _AnswerController.GetAnswersCount();
            var count = (countresult as OkNegotiatedContentResult<int>).Content;

            //act
            var result = await _AnswerController.Delete(1);
            var content = result as System.Web.Http.Results.OkResult;

            var newcountresult = await _AnswerController.GetAnswersCount();
            var newcount = (newcountresult as OkNegotiatedContentResult<int>).Content;

            //assert

            //NUnit.Framework.Assert.Null(content);
            //NUnit.Framework.Assert.AreNotEqual(count - 1, newcount);

            NUnit.Framework.Assert.NotNull(content);
            NUnit.Framework.Assert.AreEqual(count - 1, newcount);

        }

        #endregion delete answers
    }
}
