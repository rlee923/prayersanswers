﻿using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using PrayersAnswers.Api;
using PrayersAnswers.Models;
using PrayersAnswers.Tests.Fakes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Routing;

namespace PrayersAnswers.Tests.Api
{
    [TestFixture]
    class PrayerControllerTest
    {
        FakeDataRepo _repo;
        PrayerController _PrayerController;
        

        //[OneTimeSetUp]
        [SetUp]
        public void FixtureSetUp()
        {
            List<Prayer> Prayers = FakeData.CreateFakePrayers();
            List<Answer> Answers = FakeData.CreateFakeAnswers(Prayers);
            UserManager<ApplicationUser> UserManager = FakeData.CreateFakeUserManager();
            _repo = new FakeDataRepo(Prayers, Answers,UserManager);
            _PrayerController = new PrayerController(_repo);
            //_AnswerController = new AnswerController(_repo);
        }

        #region testing new prayers

        [Test]
        public async Task Add_New_Prayer_With_Matching_User_adds_one_more_prayer()
        {
            //arrange
            var identity = new GenericIdentity("abc");

            _PrayerController.User = new GenericPrincipal(identity, null);

            var result = _PrayerController.GetPrayersCount();
            var content = result.Result as System.Web.Http.Results.OkNegotiatedContentResult<int>;

            int CurrentPrayerCount = content.Content;


            //act
            PrayerVM NewPrayer = new PrayerVM();


            NewPrayer.UserID = "abc";
            NewPrayer.Title = "test title";
            NewPrayer.Notes = "new notes";


            var postresult = await _PrayerController.Post(NewPrayer);

            result = _PrayerController.GetPrayersCount();
            content = result.Result as System.Web.Http.Results.OkNegotiatedContentResult<int>;
            int NewCount = content.Content;

            //assert

            NUnit.Framework.Assert.AreEqual(CurrentPrayerCount + 1, NewCount);

        }

        [Test]
        public async Task Add_New_Prayer_Without_Matching_User_Return_null_content()
        {
            //arrange
            var identity = new GenericIdentity("bcd");

            _PrayerController.User = new GenericPrincipal(identity, null);


            var result = _PrayerController.GetPrayersCount();
            var content = result.Result as System.Web.Http.Results.OkNegotiatedContentResult<int>;

            int CurrentPrayerCount = content.Content;


            //act
            PrayerVM NewPrayer = new PrayerVM();


            NewPrayer.UserID = "abc";
            NewPrayer.Title = "test title";
            NewPrayer.Notes = "new notes";

            var postresult = await _PrayerController.Post(NewPrayer);

            var Newcontent = postresult as System.Web.Http.Results.BadRequestResult;
            //int NewCount = content.Content.Count;

            //assert

            NUnit.Framework.Assert.IsNotNull(Newcontent);

        }

        [Test]
        public async Task Add_New_Prayer_Without_User_Returns_BadRequest()
        {
            //arrange
            var identity = new GenericIdentity("");
            _PrayerController.User = new GenericPrincipal(identity, null);

            //act
            PrayerVM NewPrayer = new PrayerVM();


            NewPrayer.UserID = "abc";
            NewPrayer.Title = "test title";
            NewPrayer.Notes = "new notes";
            var postresult = await _PrayerController.Post(NewPrayer);
            var content = postresult as System.Web.Http.Results.BadRequestResult;

            //assert
            NUnit.Framework.Assert.IsNotNull(content);



        }

        #endregion testing new prayers

        #region test get prayers

        [Test]

        public void Get_Prayers_With_No_User_Returns_BadRequest()
        {
            //arrange
            var identity = new GenericIdentity("");
            _PrayerController.User = new GenericPrincipal(identity, null);

            //act
            var result = _PrayerController.GetPrayersByPage();
            var content = result.Result as System.Web.Http.Results.BadRequestResult;

            //assert
            NUnit.Framework.Assert.IsNotNull(content);


        }

        [Test]
        public void Get_Prayers_With_User_abc_returns_more_than_1()
        {
            //arrange
            var identity = new GenericIdentity("abc");
            _PrayerController.User = new GenericPrincipal(identity, null);

            //act
            var result = _PrayerController.GetPrayersCount();
            //var content = result.Result as System.Web.Http.Results.OkNegotiatedContentResult<System.Collections.Generic.List<PrayersAnswers.Models.Prayer>>;
            var content = result.Result as System.Web.Http.Results.OkNegotiatedContentResult<int>;

            //assert
            NUnit.Framework.Assert.IsNotNull(content);
            NUnit.Framework.Assert.GreaterOrEqual(content.Content, 1);
        }

        [Test]
        public async Task Get_Prayers_2nd_page_with_user_abc_returns_10_with_matching_indexes()
        {
            //arrange
            var identity = new GenericIdentity("abc");
            _PrayerController.User = new GenericPrincipal(identity, null);

            //act
            var result = await _PrayerController.GetPrayersByPage(2, 10);
            var content = result as System.Web.Http.Results.OkNegotiatedContentResult<System.Collections.Generic.List<PrayersAnswers.Models.Prayer>>;

            //assert
            int i = 11;
            foreach (var ThisPrayer in content.Content)
            {

                NUnit.Framework.Assert.IsTrue(ThisPrayer.Title.Contains(i.ToString()));
                i++;
            }
        }

        [Test]
        public async Task Get_Prayers_Without_User_returns_badrequest()
        {
            //arrange
            var identity = new GenericIdentity("");
            _PrayerController.User = new GenericPrincipal(identity, null);

            //act
            var result = await _PrayerController.GetPrayersByPage(2, 10);
            var content = result as System.Web.Http.Results.BadRequestResult;


            //assert
            NUnit.Framework.Assert.NotNull(content);
        }

        [Test]
        public async Task Get_Prayers_With_User_abc_Without_PageNum_returns_first_10()
        {
            //arrange
            var identity = new GenericIdentity("abc");
            _PrayerController.User = new GenericPrincipal(identity, null);

            //act
            var result = await _PrayerController.GetPrayersByPage(ItemsPerPage: 10);
            var content = result as System.Web.Http.Results.OkNegotiatedContentResult<System.Collections.Generic.List<PrayersAnswers.Models.Prayer>>;

            //assert
            int i = 1;
            foreach (var ThisPrayer in content.Content)
            {

                NUnit.Framework.Assert.IsTrue(ThisPrayer.Title.Contains(i.ToString()));
                i++;
            }
        }

        [Test]
        public async Task Get_Prayers_With_User_abc_Without_ItemsPerPage_returns_10()
        {
            //arrange
            var identity = new GenericIdentity("abc");
            _PrayerController.User = new GenericPrincipal(identity, null);

            //act
            var result = await _PrayerController.GetPrayersByPage(PageNum:1);
            var content = result as System.Web.Http.Results.OkNegotiatedContentResult<System.Collections.Generic.List<PrayersAnswers.Models.Prayer>>;

            //assert
            NUnit.Framework.Assert.AreEqual(content.Content.Count, 10);

        }


        #endregion get prayers


        #region edit prayers

        // edit without user login
        [Test]
        public async Task Update_Prayer_Without_User_Returns_BadReq()
        {
            //arrange
            var identity = new GenericIdentity("");
            _PrayerController.User = new GenericPrincipal(identity, null);

            PrayerVM ItemToUpdate = new PrayerVM();
            ItemToUpdate.Notes = "modified";
            ItemToUpdate.Title = "modified";
            ItemToUpdate.Id = 1;
            ItemToUpdate.UserID = "abc";


            //act
            var result = await _PrayerController.Put(ItemToUpdate);
            var content = result as System.Web.Http.Results.BadRequestResult;


            //assert
            NUnit.Framework.Assert.NotNull(content);
        }

        // edit with user login not matching userid
        [Test]
        public async Task Update_Prayer_With_non_matching_Username_Returns_BadReq()
        {
            //arrange
            var identity = new GenericIdentity("def");
            _PrayerController.User = new GenericPrincipal(identity, null);

            PrayerVM ItemToUpdate = new PrayerVM();
            ItemToUpdate.Notes = "modified";
            ItemToUpdate.Title = "modified";
            ItemToUpdate.Id = 1;
            ItemToUpdate.UserID = "abc";


            //act
            var result = await _PrayerController.Put(ItemToUpdate);
            var content = result as System.Web.Http.Results.BadRequestResult;


            //assert
            NUnit.Framework.Assert.NotNull(content);
        }

        // edit with user login, matching userid
        [Test]
        public async Task Update_Prayer_With_Matching_Username_Returns_Changed_Record()
        {
            //arrange
            var identity = new GenericIdentity("abc");
            _PrayerController.User = new GenericPrincipal(identity, null);

            PrayerVM ItemToUpdate = new PrayerVM();
            ItemToUpdate.Notes = "modified1";
            ItemToUpdate.Title = "modified2";
            ItemToUpdate.Id = 1;
            ItemToUpdate.UserID = "abc";


            //act
            var result = await _PrayerController.Put(ItemToUpdate);
            var content = result as System.Web.Http.Results.OkNegotiatedContentResult<Prayer>;



            //assert
            NUnit.Framework.Assert.AreEqual(content.Content.Title, "modified2");
            NUnit.Framework.Assert.AreEqual(content.Content.Notes, "modified1");
        }

        #endregion edit prayers


        #region delete prayers

        //test without user
        [Test]
        public async Task Delete_Prayer_Without_User_Returns_BadReq()
        {
            //arrange
            var identity = new GenericIdentity("");
            _PrayerController.User = new GenericPrincipal(identity, null);

            //act
            var result = await _PrayerController.Delete(1);
            var content = result as System.Web.Http.Results.BadRequestResult;


            //assert
            NUnit.Framework.Assert.NotNull(content);


        }

        //test with user, non-matching userId
        [Test]
        public async Task Delete_Prayer_With_User_abc_non_matching_username_BadReq()
        {

            //arrange
            var identity = new GenericIdentity("abc");
            _PrayerController.User = new GenericPrincipal(identity, null);
            
            //act
            var result = await _PrayerController.Delete(50);
            var content = result as System.Web.Http.Results.BadRequestErrorMessageResult;

            //assert
            NUnit.Framework.Assert.NotNull(content);
        }

        //test with user, matching userId
        [Test]
        public async Task Delete_Prayer_With_User_abc_matching_username_returns_ok()
        {
            //arrange
            var identity = new GenericIdentity("abc");
            _PrayerController.User = new GenericPrincipal(identity, null);
            var countresult = await _PrayerController.GetPrayersCount();
            var count = (countresult as OkNegotiatedContentResult<int>).Content;

            //act
            var result = await _PrayerController.Delete(1);
            var content = result as System.Web.Http.Results.OkResult;
            
            var newcountresult = await _PrayerController.GetPrayersCount();
            var newcount = (newcountresult as OkNegotiatedContentResult<int>).Content;

            //assert
            NUnit.Framework.Assert.NotNull(content);
            NUnit.Framework.Assert.AreEqual(count-1 , newcount);

        }

        #endregion delete prayers

    }
}
