﻿using Microsoft.AspNet.Identity;
using Moq;
using PrayersAnswers.Api.Entities;
using PrayersAnswers.Api.Models;
using PrayersAnswers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PrayersAnswers.Tests.Fakes
{
    class FakeData
    {
        static public List<Answer> CreateFakeAnswers(List<Prayer> Prayers)
        {
            List<Answer> Answers = new List<Answer>();
            for (int i = 1; i <= 25; i++)
            {
                Answer NewAnswer = new Answer();
                NewAnswer.Id = i;
                NewAnswer.UserID = "abc";
                NewAnswer.Notes = "test " + i.ToString();
                NewAnswer.DateAnswered = DateTime.Now;

                Prayer NewPrayer1 = Prayers.SingleOrDefault(x => x.Id == i);

                NewAnswer.AnswerFor = NewPrayer1;

                Answers.Add(NewAnswer);
            }
            for (int i = 26; i <= 30; i++)
            {
                ;
            }

            for (int i = 31; i <= 45; i++)
            {
                Answer NewAnswer = new Answer();
                NewAnswer.Id = i;
                NewAnswer.UserID = "def";
                NewAnswer.Notes = "test " + i.ToString();
                NewAnswer.DateAnswered = DateTime.Now;
                Answers.Add(NewAnswer);
            }

            return Answers;


        }

        static public List<Prayer> CreateFakePrayers()
        {
            List<Prayer> Prayers = new List<Prayer>();

            for (int i = 1; i <= 30; i++)
            {
                Prayer NewPrayer1 = new Prayer();
                NewPrayer1.Id = i;
                NewPrayer1.UserID = "abc";
                NewPrayer1.Title = "new prayer title for abc " + i.ToString();
                NewPrayer1.Notes = " new prayer1 notes " + i.ToString();
                NewPrayer1.DateStarted = DateTime.Now;
                Prayers.Add(NewPrayer1);
            }


            for (int i = 31; i <= 45; i++)
            {
                Prayer NewPrayer = new Prayer();
                NewPrayer.Id = i;
                NewPrayer.UserID = "def";
                NewPrayer.Title = "new prayer title";
                NewPrayer.Notes = "new prayer notes !!!";
                NewPrayer.DateStarted = DateTime.Now;
                Prayers.Add(NewPrayer);
            }


            return Prayers;
        }

        static public UserManager<ApplicationUser> CreateFakeUserManager()
        {

            var mockStore = new Mock<IUserStore<ApplicationUser>>();
            var passwordManager = mockStore
                .As<IUserPasswordStore<ApplicationUser>>();
               
            //var emailmanager = mockStore.As<IUserEmailStore<ApplicationUser>>();
            var _userManager = new UserManager<ApplicationUser>(mockStore.Object);

            


            var dummyUser1 = new ApplicationUser() { UserName = "PinkWarrior", Email = "PinkWarrior@PinkWarrior.com" };
            var dummyUser2 = new ApplicationUser() { UserName = "admin", Email = "rlee923@gmail.com" };

            var dummyUserModel = new ApplicationUser() { UserName = "abc", Email="rlee923@gmail.com"};
            var dummyUserModel2 = new ApplicationUser() { UserName = "def", Email = "rlee923@gmail.com" };


            mockStore.Setup(x => x.CreateAsync(dummyUserModel))
                .Returns(Task.FromResult(IdentityResult.Success));

            mockStore.Setup(x => x.CreateAsync(dummyUserModel2))
                .Returns(Task.FromResult(IdentityResult.Success));

            mockStore.Setup(x => x.CreateAsync(dummyUser1))
                .Returns(Task.FromResult(IdentityResult.Success));

            mockStore.Setup(x => x.CreateAsync(dummyUser2))
                .Returns(Task.FromResult(IdentityResult.Success));

            


            mockStore.Setup(x => x.FindByNameAsync(dummyUser2.UserName))
                        .Returns(Task.FromResult(dummyUser1));

            mockStore.Setup(x => x.FindByNameAsync(dummyUser2.UserName))
                        .Returns(Task.FromResult(dummyUser2));

            mockStore.Setup(x => x.FindByNameAsync(dummyUser2.UserName))
                        .Returns(Task.FromResult(dummyUser1));

            mockStore.Setup(x => x.FindByNameAsync(dummyUser2.UserName))
                        .Returns(Task.FromResult(dummyUser2));

            

            return _userManager;
        }

        static public List<Client> CreateFakeClients()
        {
            List<Client> Clients = new List<Client>();

            return Clients;
        }

        static public List<RefreshToken> CreateFakeRefreshTokens()
        {
            List<RefreshToken> RefreshTokens = new List<RefreshToken>();

            return RefreshTokens;
        }
    }
}
