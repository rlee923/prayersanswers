﻿using PrayersAnswers.Api.Models;
using PrayersAnswers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Moq;
using PrayersAnswers.Api.Entities;

namespace PrayersAnswers.Tests.Fakes
{
    class FakeDataRepo : IPrayersRepo
    {
        private List<Prayer> _Prayers;
        private List<Answer> _Answers;
        private UserManager<ApplicationUser> _userManager;
        private List<Client> _Clients;
        private List<RefreshToken> _RefreshTokens;


        public FakeDataRepo(List<Prayer> Prayers, List<Answer> Answers, UserManager<ApplicationUser> UserManager)
        {
            _Prayers = Prayers;
            _Answers = Answers;
            _userManager = UserManager;
            
        }



        public FakeDataRepo(List<Prayer> Prayers, List<Answer> Answers, UserManager<ApplicationUser> UserManager, List<Client> Clients, List<RefreshToken> RefreshTokens)
        {
            _Prayers = Prayers;
            _Answers = Answers;
            _userManager = UserManager;
            _Clients = Clients;
            _RefreshTokens = RefreshTokens;
        }

        #region authentication

        public async Task<IdentityResult> RegisterUser(UserModel userModel)
        {
            ApplicationUser user = new ApplicationUser
            {
                UserName = userModel.UserName,
                Email = userModel.EmailAddress,
                EmailConfirmed = true

                
            };

            if (userModel.Password != userModel.ConfirmPassword)
            {
                return IdentityResult.Failed();
            }

            try
            {

                var result = await _userManager.CreateAsync(user, userModel.Password);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }

            
        }

        

        public async Task<ApplicationUser> FindUser(string userName, string password)
        {
            ApplicationUser user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public Client FindClient(string clientId)
        {
            var client = _Clients.Where(x => x.Id == clientId).SingleOrDefault();

            return client;
        }

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {

            var existingToken = _RefreshTokens.Where(r => r.Subject == token.Subject && r.ClientId == token.ClientId).SingleOrDefault();

            if (existingToken != null)
            {
                var result = await RemoveRefreshToken(existingToken);
            }

            _RefreshTokens.Add(token);

            return true;
        }

        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
            var refreshToken =  _RefreshTokens.Where(x => x.Id==refreshTokenId).SingleOrDefault();

            if (refreshToken != null)
            {
                _RefreshTokens.Remove(refreshToken);
                return true;
            }

            return false;
        }

        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            _RefreshTokens.Remove(refreshToken);
            return true;
        }

        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = _RefreshTokens.Where(x => x.Id == refreshTokenId).SingleOrDefault();

            return refreshToken;
        }

        public List<RefreshToken> GetAllRefreshTokens()
        {
            return _RefreshTokens.ToList();
        }

        public async Task<ApplicationUser> FindAsync(UserLoginInfo loginInfo)
        {
            ApplicationUser user = await _userManager.FindAsync(loginInfo);

            return user;
        }

        public async Task<IdentityResult> CreateAsync(ApplicationUser user)
        {
            var result = await _userManager.CreateAsync(user);

            return result;
        }

        public async Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            var result = await _userManager.AddLoginAsync(userId, login);

            return result;
        }


        #endregion authentication



        #region prayers

        public async Task<Prayer> AddPrayer(Prayer NewPrayer)
        {
            _Prayers.Add(NewPrayer);

            try
            {

                return NewPrayer;
            }
            catch
            {
                return null;
            }


        }

        public async Task<int> GetPrayersCount(string Username)
        {
            return _Prayers.Where(x => x.UserID == Username).ToList().Count;
        }


        public async Task<List<Prayer>> GetPrayers(string Username)
        {
            var result = _Prayers.Where(x => x.UserID == Username).ToList();
            return result;

        }



        public async Task<Prayer> GetPrayer(string Username, int PrayerID)
        {
            var result = _Prayers
                .FirstOrDefault(x => x.UserID == Username && x.Id == PrayerID);

            return result;

        }

        public async Task<PrayerWithAnswer> GetPrayerWithAnswer(string Username, int PrayerID)
        {
            var result = _Prayers
                .FirstOrDefault(x => x.UserID == Username && x.Id == PrayerID);
            var CurrentAnswer = _Answers
                .Where(x => x.AnswerFor != null)
                .SingleOrDefault(x => x.AnswerFor.Id == PrayerID);
            return new PrayerWithAnswer { Prayer = result, Answer = CurrentAnswer };

        }


        public async Task<List<Prayer>> GetPrayersByPage(string Username, int ItemsPerPage = 10, int PageNum = 1)
        {
            var result = _Prayers.Where(x => x.UserID == Username)
                        .Skip((PageNum - 1) * ItemsPerPage)
                        .Take(ItemsPerPage)
                        .ToList();
            return result;

        }

        public async Task<List<PrayerWithAnswer>> GetPrayersWithAnswersByPage(string Username, int ItemsPerPage = 10, int PageNum = 1)
        {
            try
            {


                //var result = await _db.Prayers.Where(x => x.UserID == Username)
                //            .OrderBy(x => x.Id)
                //            .Skip((PageNum - 1) * ItemsPerPage)
                //            .Take(ItemsPerPage)
                //            .ToListAsync();
                var result = 
                            (from prayers in _Prayers
                             join answers in _Answers on prayers.Id equals answers.AnswerFor.Id into pa
                             from answers in pa.DefaultIfEmpty()
                             where (prayers.UserID == Username) && (answers.UserID == Username)


                             select new PrayerWithAnswer(prayers, answers))
                             .OrderBy(x => x.Prayer.Id)
                            .Skip((PageNum - 1) * ItemsPerPage)
                            .Take(ItemsPerPage)
                            .ToList();


                return result;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<Prayer> UpdatePrayer(PrayerVM CurrentPrayer, string Username)
        {
            if (CurrentPrayer.UserID != Username)
                return null;

            var RecordToChange = _Prayers.FirstOrDefault(x => x.Id == CurrentPrayer.Id && x.UserID==CurrentPrayer.UserID);
            if (RecordToChange != null)
            {
                RecordToChange.Notes = CurrentPrayer.Notes;
                RecordToChange.Title = CurrentPrayer.Title;

            }

            var index = _Prayers.FindIndex(x => x.Id == CurrentPrayer.Id);
            _Prayers[index] = RecordToChange;

            try
            {


                return RecordToChange;
            }
            catch
            {
                return null;
            }

        }

        public async Task<int> DeletePrayer(int PrayerId, string Username)
        {

            try
            {
                var AnswerToDelete = _Answers.FirstOrDefault(x => x.AnswerFor.Id == PrayerId);

                if (AnswerToDelete != null)
                    _Answers.Remove(AnswerToDelete);

                var ItemToDelete = _Prayers.FirstOrDefault(x => x.Id == PrayerId && x.UserID == Username);

                if (ItemToDelete != null)
                {

                    _Prayers.Remove(ItemToDelete);
                    return 1;
                }
                else
                    return 0;




            }


            catch (Exception)
            {
                return 0;
            }


        }


        #endregion prayers


        #region answers


        public async Task<List<Answer>> GetAnswers(string Username)
        {
            var result = _Answers.Where(x => x.UserID == Username).ToList();
            return result;

        }

        public async Task<Answer> GetAnswer(string Username,int Id)
        {
            var result =  _Answers.SingleOrDefault(x => x.Id == Id && x.UserID == Username);
            return result;

        }

        public async Task<int> GetAnswersCount(string Username)
        {
            var result =  _Answers.Where(x => x.UserID == Username).ToList();
            return result.Count();
        }

        public async Task<Answer> AddAnswer(Answer NewAnswer)
        {
            _Answers.Add(NewAnswer);

            return NewAnswer;


        }

        public async Task<List<Answer>> GetAnswersByPage(string Username, int ItemsPerPage = 10, int PageNum = 1)
        {
            var result =  _Answers.Where(x => x.UserID == Username)
                        .Skip((PageNum - 1) * ItemsPerPage)
                        .Take(ItemsPerPage)
                        .ToList();
            return result;

        }

        public async Task<Answer> UpdateAnswer(AnswerVM CurrentAnswer, string Username)
        {
            if (CurrentAnswer.UserID != Username)
                return null;

            var RecordToChange =  _Answers.FirstOrDefault(x => x.Id == CurrentAnswer.Id && x.UserID == CurrentAnswer.UserID);
            if (RecordToChange != null)
            {
                RecordToChange.Notes = CurrentAnswer.Notes;
                RecordToChange.Type = CurrentAnswer.Type;

            }

            return RecordToChange;

        }


        public async Task<int> DeleteAnswer(int AnswerId, string Username)
        {

            try
            {
                var ItemToDelete = _Answers.FirstOrDefault(x => x.Id == AnswerId && x.UserID == Username);

                if (ItemToDelete != null)
                {
                    _Answers.Remove(ItemToDelete);
                    
                    return 1;
                }
                else
                    return 0;


            }


            catch (Exception)
            {
                return 0;
            }


        }

        public async Task<Answer> FindAnswerForPrayer(int PrayerID)
        {
            Answer CurrentAnswer = _Answers
                .Where(x => x.AnswerFor != null)
                .SingleOrDefault(x => x.AnswerFor.Id == PrayerID);

            return CurrentAnswer;
        }

        #endregion answers

    }
}
