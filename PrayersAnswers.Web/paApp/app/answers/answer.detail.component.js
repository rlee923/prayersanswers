var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { DataService } from './../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CheckAuthService } from './../services/checkauth.service';
var AnswerDetailComponent = (function () {
    function AnswerDetailComponent(_route, _dataService, _router, _checkAuthService, _localStorageService) {
        this._route = _route;
        this._dataService = _dataService;
        this._router = _router;
        this._checkAuthService = _checkAuthService;
        this._localStorageService = _localStorageService;
        this._title = "";
        this._msg = "";
        this._type = "";
        this._success = false;
    }
    AnswerDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._checkAuthService.handleError("", this._router);
        this.sub = this._route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.loadAnswerData();
        });
    };
    AnswerDetailComponent.prototype.loadAnswerData = function () {
        var _this = this;
        this._dataService.getAnswer(this.id)
            .subscribe((function (res) {
            _this._answer = res;
            _this._title = _this._answer.answerFor.title;
            _this._notes = _this._answer.notes;
            _this._type = _this._answer.type;
            if (_this._type == "0")
                _this._type = "";
        }), (function (error) { _this.handleFailed(error); }));
    };
    AnswerDetailComponent.prototype.editAnswerData = function () {
        var _this = this;
        if (this._type == "0" || this._type == "") {
            this._success = false;
            this._msg = "type is required";
            return;
        }
        var answer = {
            id: this._answer.id,
            userId: this._localStorageService.get('auth').userName,
            notes: this._notes,
            type: this._type
        };
        this._dataService.editAnswer(answer)
            .subscribe((function (res) {
            _this._msg = "Details saved, redirecting in 5 seconds";
            setTimeout(function (router) {
                _this._router.navigate(['/answers']);
            }, 5000);
        }), (function (error) { _this.handleFailed(error); }));
    };
    AnswerDetailComponent.prototype.handleFailed = function (error) {
        this._msg = error;
        this._checkAuthService.handleError(error, this._router);
    };
    return AnswerDetailComponent;
}());
AnswerDetailComponent = __decorate([
    Component({
        selector: 'answer-detail',
        templateUrl: 'answer.detail.component.html',
    }),
    __metadata("design:paramtypes", [ActivatedRoute,
        DataService,
        Router,
        CheckAuthService,
        LocalStorageService])
], AnswerDetailComponent);
export { AnswerDetailComponent };
//# sourceMappingURL=answer.detail.component.js.map