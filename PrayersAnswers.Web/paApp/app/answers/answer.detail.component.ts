﻿import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { DataService } from './../services/data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { IAuthStatus } from './../services/authStatus';
import { CheckAuthService } from './../services/checkauth.service';


@Component({
    selector: 'answer-detail',
    templateUrl: 'answer.detail.component.html',


})

export class AnswerDetailComponent implements OnInit {

    private sub: any;
    id: number;
    private _answer: any;
    private _title: string = "";
    private _notes: string;
    private _msg: string = "";
    private _type: string = "";
    private _success: boolean = false;


    constructor(private _route: ActivatedRoute,
        private _dataService: DataService,
        private _router: Router,
        private _checkAuthService: CheckAuthService,
        private _localStorageService: LocalStorageService
    ) {

    }

    ngOnInit() {

        this._checkAuthService.handleError("", this._router);

        this.sub = this._route.params.subscribe((params) => {
            this.id = +params['id']; // (+) converts string 'id' to a number

            // load data here

            this.loadAnswerData();
        });

    }

    loadAnswerData() {
        this._dataService.getAnswer(this.id)
            .subscribe(
            ((res) => {
                this._answer = res;
                this._title = this._answer.answerFor.title;
                this._notes = this._answer.notes;
                this._type = this._answer.type;
                if (this._type == "0")
                    this._type = "";
            })
            , ((error) => { this.handleFailed(error) })
            )

    }

    editAnswerData() {

        if (this._type == "0" || this._type == "")
        {
            this._success = false;
            this._msg="type is required"
            return;
        }

        let answer = {
            id: this._answer.id,
            userId: this._localStorageService.get('auth').userName,            
            notes: this._notes,
            type: this._type
        }

        this._dataService.editAnswer(answer)
            .subscribe(
            ((res) => {
                

                this._msg = "Details saved, redirecting in 5 seconds";
                setTimeout((router: Router) => {
                    this._router.navigate(['/answers']);
                }, 5000)


            })
            , ((error) => { this.handleFailed(error) })
            )
    }

    


    handleFailed(error: any) {

        this._msg = error;
        this._checkAuthService.handleError(error, this._router);

    }



}