﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AnswerDetailComponent } from './answer.detail.component';
import { RouterModule } from '@angular/router';


@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,
        RouterModule


    ],

    declarations: [
        AnswerDetailComponent
    ],
    exports: [
        AnswerDetailComponent
    ]

})

export class AnswerDetailModule { }