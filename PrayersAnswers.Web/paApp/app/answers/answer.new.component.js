var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { DataService } from './../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CheckAuthService } from './../services/checkauth.service';
var AnswerNewComponent = (function () {
    function AnswerNewComponent(_route, _dataService, _router, _checkAuthService, _localStorageService) {
        this._route = _route;
        this._dataService = _dataService;
        this._router = _router;
        this._checkAuthService = _checkAuthService;
        this._localStorageService = _localStorageService;
        this._title = "";
        this._msg = "";
        this._type = "";
        this._success = false;
    }
    AnswerNewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._checkAuthService.handleError("", this._router);
        this.sub = this._route.params.subscribe(function (params) {
            _this._prayerId = +params['prayerId'];
            _this.loadPrayerData();
        });
    };
    AnswerNewComponent.prototype.loadPrayerData = function () {
        var _this = this;
        this._dataService.getPrayer(this._prayerId)
            .subscribe((function (res) {
            _this._prayer = res;
            _this._title = _this._prayer.title;
        }), (function (error) { _this.handleFailed(error); }));
    };
    AnswerNewComponent.prototype.addAnswerData = function () {
        var _this = this;
        if (this._type == "0" || this._type == "") {
            this._success = false;
            this._msg = "type is required";
            return;
        }
        var answer = {
            userId: this._localStorageService.get('auth').userName,
            notes: this._notes,
            prayerId: this._prayerId,
            type: this._type
        };
        this._dataService.addAnswer(answer)
            .subscribe((function (res) {
            _this._success = true;
            _this._msg = "Details saved, redirecting to the previous answer detail page 5 seconds";
            setTimeout(function (router) {
                _this._router.navigate(['/prayer-detail', _this._prayerId]);
            }, 5000);
        }), (function (error) { _this.handleFailed(error); }));
    };
    AnswerNewComponent.prototype.handleFailed = function (error) {
        this._msg = error;
        this._checkAuthService.handleError(error, this._router);
    };
    return AnswerNewComponent;
}());
AnswerNewComponent = __decorate([
    Component({
        selector: 'answer-new',
        templateUrl: 'answer.new.component.html',
    }),
    __metadata("design:paramtypes", [ActivatedRoute,
        DataService,
        Router,
        CheckAuthService,
        LocalStorageService])
], AnswerNewComponent);
export { AnswerNewComponent };
//# sourceMappingURL=answer.new.component.js.map