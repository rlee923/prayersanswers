﻿import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { DataService } from './../services/data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { IAuthStatus } from './../services/authStatus';
import { CheckAuthService } from './../services/checkauth.service';


@Component({
    selector: 'answer-new',
    templateUrl: 'answer.new.component.html',


})

export class AnswerNewComponent implements OnInit {

    private sub: any;
    _prayerId: number;
    private _answer: any;
    private _prayer: any;
    private _title: string = "";
    private _notes: string;
    private _msg: string = "";
    private _type: string = "";
    private _success: boolean = false;


    constructor(private _route: ActivatedRoute,
        private _dataService: DataService,
        private _router: Router,
        private _checkAuthService: CheckAuthService,
        private _localStorageService: LocalStorageService
    ) {

    }

    ngOnInit() {

        this._checkAuthService.handleError("", this._router);

        this.sub = this._route.params.subscribe((params) => {
            this._prayerId = +params['prayerId']; // (+) converts string 'id' to a number

            // load data here
            this.loadPrayerData();

        });

    }

    loadPrayerData() {
        this._dataService.getPrayer(this._prayerId)
            .subscribe(
            ((res) => {
                this._prayer = res;
                this._title = this._prayer.title;
                
            })
            , ((error) => { this.handleFailed(error) })
            )
    }


   

    addAnswerData() {

        if (this._type == "0" || this._type == "") {
            this._success = false;
            this._msg = "type is required"
            return;
        }

        let answer = {
            
            userId: this._localStorageService.get('auth').userName,
            notes: this._notes,
            prayerId: this._prayerId,
            type: this._type
        }

        this._dataService.addAnswer(answer)
            .subscribe(
            ((res) => {

                this._success = true;
                this._msg = "Details saved, redirecting to the previous answer detail page 5 seconds";
                setTimeout((router: Router) => {
                    this._router.navigate(['/prayer-detail', this._prayerId]);
                }, 5000)


            })
            , ((error) => { this.handleFailed(error) })
            )
    }




    handleFailed(error: any) {

        this._msg = error;
        this._checkAuthService.handleError(error, this._router);

    }



}