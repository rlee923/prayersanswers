var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AnswerNewComponent } from './answer.new.component';
import { RouterModule } from '@angular/router';
var AnswerNewModule = (function () {
    function AnswerNewModule() {
    }
    return AnswerNewModule;
}());
AnswerNewModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            BrowserModule,
            FormsModule,
            RouterModule
        ],
        declarations: [
            AnswerNewComponent
        ],
        exports: [
            AnswerNewComponent
        ]
    })
], AnswerNewModule);
export { AnswerNewModule };
//# sourceMappingURL=answer.new.module.js.map