﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AnswerNewComponent } from './answer.new.component';
import { RouterModule } from '@angular/router';


@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,
        RouterModule


    ],

    declarations: [
        AnswerNewComponent
    ],
    exports: [
        AnswerNewComponent
    ]

})

export class AnswerNewModule { }