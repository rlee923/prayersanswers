var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { DataService } from './../services/data.service';
import { Router } from '@angular/router';
import { PagerService } from './../services/page.service';
import { CheckAuthService } from './../services/checkauth.service';
var AnswersComponent = (function () {
    function AnswersComponent(_localStorageService, _router, _dataService, _pagerService, _checkAuthService) {
        this._localStorageService = _localStorageService;
        this._router = _router;
        this._dataService = _dataService;
        this._pagerService = _pagerService;
        this._checkAuthService = _checkAuthService;
        this._msg = "";
        this._pager = {};
    }
    AnswersComponent.prototype.ngOnInit = function () {
        this._checkAuthService.handleError("", this._router);
        this.setPage(1);
    };
    AnswersComponent.prototype.getAnswers = function () {
        var _this = this;
        this._dataService.getAnswersByPage().subscribe((function (res) {
            _this._answers = res;
        }), (function (error) { _this.handleFailed(error); }));
    };
    AnswersComponent.prototype.getTotalItemCount = function () {
        var _this = this;
        var totalItemCount = 0;
        this._dataService.getAnswersCount().subscribe((function (res) {
            totalItemCount = res;
        }), (function (error) { _this.handleFailed(error); }));
        return totalItemCount;
    };
    AnswersComponent.prototype.getAnswersByPage = function (pageNum) {
        var _this = this;
        var totalItemCount = 0;
        this._dataService.getAnswersByPage(pageNum).subscribe((function (res) {
            _this._answers = res;
        }), (function (error) { _this.handleFailed(error); }));
        return totalItemCount;
    };
    AnswersComponent.prototype.setPage = function (page) {
        var _this = this;
        if (page < 1 || page > this._pager.totalPages) {
            return;
        }
        var totalItemCount = 0;
        this._dataService.getAnswersCount().subscribe((function (res) {
            totalItemCount = res;
            _this._pager = _this._pagerService.getPager(totalItemCount, page);
            _this.getAnswersByPage(page);
        }), (function (error) { _this.handleFailed(error); }));
    };
    AnswersComponent.prototype.addNewAnswer = function () {
        this._router.navigate(['/answer-new']);
    };
    AnswersComponent.prototype.handleFailed = function (error) {
        this._msg = error;
        this._checkAuthService.handleError(error, this._router);
    };
    return AnswersComponent;
}());
AnswersComponent = __decorate([
    Component({
        selector: 'answers',
        templateUrl: 'answers.component.html',
        providers: [PagerService]
    }),
    __metadata("design:paramtypes", [LocalStorageService,
        Router,
        DataService,
        PagerService,
        CheckAuthService])
], AnswersComponent);
export { AnswersComponent };
//# sourceMappingURL=answers.component.js.map