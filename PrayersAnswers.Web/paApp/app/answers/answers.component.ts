﻿import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { DataService } from './../services/data.service';
import { Router } from '@angular/router';
import { IAuthStatus } from './../services/authStatus';
import { PagerService } from './../services/page.service';
import { CheckAuthService } from './../services/checkauth.service';



@Component({
    selector: 'answers',
    templateUrl: 'answers.component.html',
    providers: [PagerService]

})

export class AnswersComponent implements OnInit {

    private _authStatus: IAuthStatus;
    private _answers: any[];
    private _msg: string = "";
    private _pager: any = {};

    constructor(private _localStorageService: LocalStorageService
        , private _router: Router
        , private _dataService: DataService
        , private _pagerService: PagerService
        , private _checkAuthService: CheckAuthService
    ) {
    }


    ngOnInit() {
        this._checkAuthService.handleError("", this._router);

        //this.getPrayers();

        this.setPage(1);
    }


    getAnswers() {

        // get total prayers count


        // call data service to get prayers
        this._dataService.getAnswersByPage().subscribe(
            ((res) => {
                this._answers = res;
            })
            , ((error) => { this.handleFailed(error) })

        )

    }

    getTotalItemCount(): number {
        let totalItemCount: number = 0;
        this._dataService.getAnswersCount().subscribe(
            ((res) => {
                totalItemCount = res;
            })
            , ((error) => { this.handleFailed(error) })

        )
        return totalItemCount;
    }

    getAnswersByPage(pageNum: number) {
        let totalItemCount: number = 0;
        this._dataService.getAnswersByPage(pageNum).subscribe(
            ((res) => {
                this._answers = res;
            })
            , ((error) => { this.handleFailed(error) })

        )
        return totalItemCount;
    }

    setPage(page: number) {
        if (page < 1 || page > this._pager.totalPages) {
            return;
        }

        let totalItemCount: number = 0;

        this._dataService.getAnswersCount().subscribe(
            ((res) => {
                totalItemCount = res;
                this._pager = this._pagerService.getPager(totalItemCount, page);
                this.getAnswersByPage(page);
            })
            , ((error) => { this.handleFailed(error) })

        )


        // get pager object from service


        // get current page of items

    }


    addNewAnswer() {
        // redirect to new prayer page
        this._router.navigate(['/answer-new']);

    }


    handleFailed(error: any) {

        this._msg = error;
        this._checkAuthService.handleError(error, this._router);

    }
}