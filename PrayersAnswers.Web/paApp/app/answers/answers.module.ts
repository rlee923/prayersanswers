﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AnswersComponent } from './answers.component';
import { RouterModule } from '@angular/router';


@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,
        RouterModule

    ],

    declarations: [
        AnswersComponent
    ],
    exports: [
        AnswersComponent
    ]

})

export class AnswersModule { }