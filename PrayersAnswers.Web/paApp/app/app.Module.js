var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Configuration } from './app.constants';
import { AppRoutes } from './app.routes';
import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { LoginModule } from './login/login.module';
import { AuthService } from './services/authService';
import { RegisterModule } from './register/register.module';
import { LocalStorageService } from './services/localstorage.service';
import { AuthCompleteModule } from './authComplete/authComplete.module';
import { DataService } from './services/data.service';
import { PrayersModule } from './prayers/prayers.module';
import { PrayerDetailModule } from './prayers/prayer.detail.module';
import { PrayerNewModule } from './prayers/prayer.new.module';
import { HttpClient } from './services/httpClient.service';
import { CheckAuthService } from './services/checkauth.service';
import { AnswersModule } from './answers/answers.module';
import { AnswerDetailModule } from './answers/answer.detail.module';
import { AnswerNewModule } from './answers/answer.new.module';
import { LoadingAnimateService } from 'ng2-loading-animate';
import { AssociateModule } from './associate/associate.module';
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    NgModule({
        imports: [
            BrowserModule,
            AppRoutes,
            FormsModule,
            HomeModule,
            HttpModule,
            LoginModule,
            CommonModule,
            RegisterModule,
            AuthCompleteModule,
            PrayersModule,
            PrayerDetailModule,
            PrayerNewModule,
            AnswersModule,
            AnswerDetailModule,
            AnswerNewModule,
            AssociateModule,
        ],
        declarations: [
            AppComponent,
        ],
        providers: [AuthService,
            Configuration,
            LocalStorageService,
            DataService,
            HttpClient,
            CheckAuthService,
            LoadingAnimateService
        ],
        bootstrap: [AppComponent],
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.Module.js.map