﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { Configuration } from './app.constants';
import { AppRoutes } from './app.routes';
import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { LoginModule } from './login/login.module';
import { AuthService } from './services/authService';
import { RegisterModule } from './register/register.module';
import { LocalStorageService } from './services/localstorage.service';
import { AuthCompleteModule } from './authComplete/authComplete.module';
import { DataService } from './services/data.service';
import { PrayersModule } from './prayers/prayers.module';
import { PrayerDetailModule } from './prayers/prayer.detail.module';
import { PrayerNewModule } from './prayers/prayer.new.module';
import { httpInterceptService } from './services/httpIntercept.service';
import { HttpClient } from './services/httpClient.service';
import { CheckAuthService } from './services/checkauth.service';
import { AnswersModule } from './answers/answers.module';
import { AnswerDetailModule } from './answers/answer.detail.module';
import { AnswerNewModule } from './answers/answer.new.module';
import { LoadingAnimateModule, LoadingAnimateService } from 'ng2-loading-animate';
import { AssociateModule } from './associate/associate.module';



@NgModule({
    imports: [
        BrowserModule,        
        AppRoutes,
        FormsModule,
        HomeModule,
        HttpModule,
        LoginModule,
        CommonModule,
        RegisterModule,
        AuthCompleteModule,
        PrayersModule,
        PrayerDetailModule,
        PrayerNewModule,
        AnswersModule,
        AnswerDetailModule,
        AnswerNewModule,
        AssociateModule,
        //LoadingAnimateModule.forRoot()

        
        //DummyModule,
        
        //SharedModule,
        //CoreModule.forRoot(),
       // LocalStorageService,
        
        
    ],

    declarations: [
        AppComponent,
        
        
        
    ],

    providers: [AuthService,
        Configuration,
        LocalStorageService,
        DataService,
        HttpClient,
        CheckAuthService,
        LoadingAnimateService
        
    ],

    bootstrap: [AppComponent],
})



export class AppModule { }