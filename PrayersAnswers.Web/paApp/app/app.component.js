var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import './app.component.scss';
import '../style/app.scss';
import { AuthService } from './services/authService';
import { LocalStorageService } from './services/localstorage.service';
import { Router } from '@angular/router';
var AppComponent = (function () {
    function AppComponent(_authService, _localStorageService, _router) {
        this._authService = _authService;
        this._localStorageService = _localStorageService;
        this._router = _router;
    }
    AppComponent.prototype.ngOnInit = function () {
        this._authStatus = { userName: "", isAuth: false, useRefreshTokens: false };
        var localAuth = this._localStorageService.get('auth');
        if (localAuth)
            this._authStatus = localAuth;
    };
    AppComponent.prototype.logOut = function () {
        this._localStorageService.remove('auth');
        this._localStorageService.remove('authData');
        this._router.navigate(['/login']);
    };
    return AppComponent;
}());
AppComponent = __decorate([
    Component({
        selector: 'my-app',
        templateUrl: 'app.component.html',
    }),
    __metadata("design:paramtypes", [AuthService,
        LocalStorageService,
        Router])
], AppComponent);
export { AppComponent };
//# sourceMappingURL=app.component.js.map