﻿import { Component, OnInit } from '@angular/core';
//import { Router } from '@angular/router'

// AoT compilation doesn't support 'require'.
import './app.component.scss';
import '../style/app.scss';
import { AuthService } from './services/authService';

import { IAuthStatus } from './services/authStatus';
import { LocalStorageService } from './services/localstorage.service';
import { Router } from '@angular/router';

@Component({
    selector: 'my-app',
    templateUrl: 'app.component.html',

})



export class AppComponent implements OnInit {



    private _authStatus: IAuthStatus;


    constructor(private _authService: AuthService
        , private _localStorageService: LocalStorageService
        , private _router:Router
        //, private _router: Router
    ) {

    }


    ngOnInit() {
        this._authStatus = { userName: "", isAuth: false, useRefreshTokens: false };

        let localAuth = this._localStorageService.get('auth');

        if (localAuth)
            this._authStatus = localAuth;

        //if (!!this._authStatus.userName) {
        //    this._router.navigate(['/login']);
        //}
    }

    logOut() {
        this._localStorageService.remove('auth');
        this._localStorageService.remove('authData');
        this._router.navigate(['/login']);
    }

}