﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthCompleteComponent } from './authComplete/authComplete.component';
import { PrayersComponent } from './prayers/prayers.component';
import { PrayerDetailComponent } from './prayers/prayer.detail.component';
import { PrayerNewComponent } from './prayers/prayer.new.component';
import { AnswersComponent } from './answers/answers.component';
import { AnswerDetailComponent } from './answers/answer.detail.component';
import { AnswerNewComponent } from './answers/answer.new.component';
import { AssociateComponent } from './associate/associate.component';

const routes: Routes = [

    //{ path: 'crisis-center', component: CrisisListComponent },
    //{ path: 'hero/:id', component: HeroDetailComponent },
    //{
    //    path: 'dummy', loadChildren: './dummy/dummy.module#DummyModule',
    //},
    { path: 'home', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'associate', component: AssociateComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'authcomplete', component: AuthCompleteComponent },
    { path: 'prayer-detail/:id', component: PrayerDetailComponent },
    { path: 'prayer-new', component: PrayerNewComponent },
    { path: 'prayers', component: PrayersComponent },
    { path: 'answers', component: AnswersComponent },
    { path: 'answer-detail/:id', component: AnswerDetailComponent },
    { path: 'answer-new/:prayerId', component: AnswerNewComponent },
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: '**', redirectTo: 'home' }
    
];

//const routes: Routes = [

    
//    {
//        path: 'dummy', component: DummyComponent,
//    },
//    { path: 'home', component: HomeComponent },
//    { path: '', redirectTo: 'home', pathMatch: 'full' },
//    { path: '**', redirectTo: 'home' }

//];



export const AppRoutes = RouterModule.forRoot(routes);

