var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from './../services/localstorage.service';
import { AuthService } from './../services/authService';
import { Configuration } from './../app.constants';
var AssociateComponent = (function () {
    function AssociateComponent(_AuthService, _router, _config, _localStorageService) {
        this._AuthService = _AuthService;
        this._router = _router;
        this._config = _config;
        this._localStorageService = _localStorageService;
        this._successful = false;
        this._registerData = {};
        this._msg = "";
        this._userName = "";
    }
    AssociateComponent.prototype.handleFailed = function (error) {
        this._successful = false;
        this._msg = error;
    };
    AssociateComponent.prototype.ngOnInit = function () {
        this._registerData = this._AuthService._externalAuthData;
        this._userName = this._registerData.userName;
    };
    AssociateComponent.prototype.registerExternal = function () {
        var _this = this;
        var newRegisterData = {
            userName: this._userName,
            provider: this._registerData.provider,
            externalAccessToken: this._registerData.externalAccessToken
        };
        this._AuthService.registerExternal(newRegisterData).subscribe((function (res) {
            window.location.href = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/home';
        }), (function (error) { _this.handleFailed(error); }));
    };
    return AssociateComponent;
}());
AssociateComponent = __decorate([
    Component({
        selector: 'associate',
        templateUrl: 'associate.component.html',
    }),
    __metadata("design:paramtypes", [AuthService,
        Router,
        Configuration,
        LocalStorageService])
], AssociateComponent);
export { AssociateComponent };
//# sourceMappingURL=associate.component.js.map