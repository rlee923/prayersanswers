﻿import { Component, OnInit, NgModule, Input } from '@angular/core';
import { Router } from '@angular/router'
import { LocalStorageService } from './../services/localstorage.service';
import { AuthService } from './../services/authService';
import { Configuration } from './../app.constants';
import { IAuthStatus } from './../services/authStatus';


@Component({
    selector: 'associate',
    templateUrl: 'associate.component.html',

})

export class AssociateComponent implements OnInit
{
    private _successful: boolean = false;
    private _registerData: any = {};
    private _msg: string = "";
    private _userName:string =""

    constructor(private _AuthService: AuthService,
        private _router: Router
        ,private _config: Configuration
        ,private _localStorageService: LocalStorageService
        //private _loadingSvc: LoadingAnimateService
    ) {

    }

    handleFailed(error: any) {
        this._successful = false;
        this._msg = error;

    }


    ngOnInit() {
        this._registerData = this._AuthService._externalAuthData;
        this._userName = this._registerData.userName;
    }

    registerExternal() {

        let newRegisterData = {
            userName: this._userName,
            provider: this._registerData.provider,
            externalAccessToken: this._registerData.externalAccessToken
        }
        this._AuthService.registerExternal(newRegisterData).subscribe(
            ((res) => {
                window.location.href = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/home'
                //this._router.navigate(['/home']);
            })
            , ((error) => { this.handleFailed(error) })
        )
        // call authdata register

        // if successful redirect

        // else display message
    }
}