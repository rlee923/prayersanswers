﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AssociateComponent } from './associate.component';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,

    ],

    declarations: [
        AssociateComponent
    ],
    exports: [
        AssociateComponent
    ]

})

export class AssociateModule { }