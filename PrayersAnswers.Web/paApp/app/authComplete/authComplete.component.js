var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Router, ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { AuthService } from './../services/authService';
var AuthCompleteComponent = (function () {
    function AuthCompleteComponent(_router, activatedRoute, _localStorageService, _AuthService) {
        this._router = _router;
        this.activatedRoute = activatedRoute;
        this._localStorageService = _localStorageService;
        this._AuthService = _AuthService;
        this._fragments = {};
        this._fragments_string = "";
    }
    AuthCompleteComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.fragment.subscribe(function (fragment) {
            _this._fragments_string = fragment;
            _this.parseFragmentsAndRedirect();
        });
    };
    AuthCompleteComponent.prototype.parseQueryString = function (queryString) {
        var data = {}, pairs, pair, separatorIndex, escapedKey, escapedValue, key, value;
        if (queryString === null) {
            return data;
        }
        if (queryString.indexOf("amp;") > 0)
            pairs = queryString.split("&amp;");
        else
            pairs = queryString.split("&");
        for (var i = 0; i < pairs.length; i++) {
            pair = pairs[i];
            separatorIndex = pair.indexOf("=");
            if (separatorIndex === -1) {
                escapedKey = pair;
                escapedValue = null;
            }
            else {
                escapedKey = pair.substr(0, separatorIndex);
                escapedValue = pair.substr(separatorIndex + 1);
            }
            key = decodeURIComponent(escapedKey);
            value = decodeURIComponent(escapedValue);
            data[key] = value;
        }
        return data;
    };
    AuthCompleteComponent.prototype.parseFragmentsAndRedirect = function () {
        var _this = this;
        this._fragments = this.parseQueryString(this._fragments_string);
        if (this._fragments.haslocalaccount == 'False') {
            this._AuthService._externalAuthData = {
                provider: this._fragments.provider,
                userName: this._fragments.external_user_name,
                externalAccessToken: this._fragments.external_access_token
            };
            this._router.navigate(['/associate']);
        }
        else {
            var externalData = { provider: this._fragments.provider, externalAccessToken: this._fragments.external_access_token };
            this._AuthService.ObtainLocalAccessToken(externalData).subscribe((function (res) {
                window.location.href = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/home';
            }), (function (error) { _this.handleFailed(error); }));
        }
    };
    AuthCompleteComponent.prototype.handleFailed = function (error) {
    };
    return AuthCompleteComponent;
}());
AuthCompleteComponent = __decorate([
    Component({
        selector: 'authComplete',
        templateUrl: 'authComplete.component.html',
    }),
    __metadata("design:paramtypes", [Router, ActivatedRoute, LocalStorageService, AuthService])
], AuthCompleteComponent);
export { AuthCompleteComponent };
//# sourceMappingURL=authComplete.component.js.map