﻿import { Router, ActivatedRoute, Params } from '@angular/router';
import { OnInit, OnDestroy, Component } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { AuthService } from './../services/authService';
// AoT compilation doesn't support 'require'.


@Component({
    selector: 'authComplete',
    templateUrl: 'authComplete.component.html',
    
})

export class AuthCompleteComponent implements  OnInit {

    private _fragments: any = {};
    private _fragments_string: string = "";

    constructor(private _router: Router,private activatedRoute: ActivatedRoute, private _localStorageService: LocalStorageService, private _AuthService: AuthService) { }

    ngOnInit() {
        // subscribe to router event
        this.activatedRoute.fragment.subscribe((fragment: string) => {
            this._fragments_string = fragment;
            //this._localStorageService.set('fragments', this._fragments);
            this.parseFragmentsAndRedirect();
        });        

        
        //window.close();
    }

    parseQueryString(queryString: any) {
        var data: any = {},
            pairs, pair, separatorIndex, escapedKey, escapedValue, key, value;

        if (queryString === null) {
            return data;
        }

        if (queryString.indexOf("amp;") > 0)
            pairs = queryString.split("&amp;");
        else
            pairs = queryString.split("&");

        for (var i = 0; i < pairs.length; i++) {
            pair = pairs[i];
            separatorIndex = pair.indexOf("=");

            if (separatorIndex === -1) {
                escapedKey = pair;
                escapedValue = null;
            } else {
                escapedKey = pair.substr(0, separatorIndex);
                escapedValue = pair.substr(separatorIndex + 1);
            }

            key = decodeURIComponent(escapedKey);
            value = decodeURIComponent(escapedValue);

            data[key] = value;
        }

        return data;
    }


    parseFragmentsAndRedirect() {
        this._fragments = this.parseQueryString(this._fragments_string);

        if (this._fragments.haslocalaccount == 'False') {

            this._AuthService._externalAuthData = {
                provider: this._fragments.provider,
                userName: this._fragments.external_user_name,
                externalAccessToken: this._fragments.external_access_token
            };

            this._router.navigate(['/associate']);

        }
        else {
            //Obtain access token and redirect to home
            var externalData = { provider: this._fragments.provider, externalAccessToken: this._fragments.external_access_token };
            this._AuthService.ObtainLocalAccessToken(externalData).subscribe(
                ((res) => {
                    //this._router.navigate(['/home']);
                    window.location.href = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/home'
                })
                , ((error) => { this.handleFailed(error) })
            )

        }


    }

    handleFailed(error: any) {
        //this._successful = false;
        //this._msg = error;

    }
    
}