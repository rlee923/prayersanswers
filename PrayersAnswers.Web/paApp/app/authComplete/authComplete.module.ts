﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AuthCompleteComponent } from './authComplete.component';



@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,
        
    ],

    declarations: [
        AuthCompleteComponent
    ],
    exports: [
        AuthCompleteComponent
    ]
    
})

export class AuthCompleteModule {}