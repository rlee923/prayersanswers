﻿import { Component } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
// AoT compilation doesn't support 'require'.


@Component({
    selector: 'dummy',
    templateUrl: 'dummy.component.html',
    
})

export class DummyComponent {

    private _testString: string = "";

    constructor(private _localStorageService: LocalStorageService) {
        this._testString = _localStorageService.get('dummyString');
    }
}