﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DummyComponent } from './dummy.component';
import { DummyRoutes } from './dummy.routes';


@NgModule({
    imports: [
        CommonModule,
        DummyRoutes
    ],

    declarations: [
        DummyComponent
    ],
    exports: [
        DummyComponent
    ]
    
})

export class DummyModule {}