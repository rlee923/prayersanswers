﻿import { Routes, RouterModule } from '@angular/router';

import { DummyComponent } from './dummy.component';

const routes: Routes = [
    { path: '', component: DummyComponent }
];

export const DummyRoutes = RouterModule.forChild(routes);