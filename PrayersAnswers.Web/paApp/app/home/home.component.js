var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { Router } from '@angular/router';
var HomeComponent = (function () {
    function HomeComponent(_localStorageService, _router) {
        this._localStorageService = _localStorageService;
        this._router = _router;
    }
    HomeComponent.prototype.ngOnInit = function () {
        this._authStatus = { userName: "", isAuth: false, useRefreshTokens: false };
        this._authStatus = this._localStorageService.get('auth');
        if (!this._authStatus)
            this._router.navigate(['/login']);
        else if ((!this._authStatus.userName || 0 === this._authStatus.userName.length)) {
            this._router.navigate(['/login']);
        }
        else {
        }
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    Component({
        selector: 'home',
        templateUrl: 'home.component.html',
    }),
    __metadata("design:paramtypes", [LocalStorageService, Router])
], HomeComponent);
export { HomeComponent };
//# sourceMappingURL=home.component.js.map