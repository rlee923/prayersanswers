﻿import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { Router } from '@angular/router';
import { IAuthStatus } from './../services/authStatus';
// AoT compilation doesn't support 'require'.


@Component({
    selector: 'home',
    templateUrl: 'home.component.html',
    
})

export class HomeComponent implements OnInit {

    private _authStatus: IAuthStatus;

    constructor(private _localStorageService: LocalStorageService, private _router: Router) {
    }


    ngOnInit() {
        this._authStatus = { userName: "", isAuth: false, useRefreshTokens: false };
        this._authStatus = this._localStorageService.get('auth');

        if (!this._authStatus)
            this._router.navigate(['/login']);

        else if ((!this._authStatus.userName || 0 === this._authStatus.userName.length)) {

            
            this._router.navigate(['/login']);
        }
        else {
            
            
        }
    }
}