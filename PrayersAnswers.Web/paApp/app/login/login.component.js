var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from './../services/localstorage.service';
import { AuthService } from './../services/authService';
import { Configuration } from './../app.constants';
import { LoadingAnimateService } from 'ng2-loading-animate';
var LoginComponent = (function () {
    function LoginComponent(_AuthService, _router, _config, _localStorageService, _loadingSvc) {
        this._AuthService = _AuthService;
        this._router = _router;
        this._config = _config;
        this._localStorageService = _localStorageService;
        this._loadingSvc = _loadingSvc;
        this._fragments = {};
        this._fragments_string = "";
    }
    LoginComponent.prototype.ngOnInit = function () {
        this._msg = "";
        var _authStatus = { userName: "", isAuth: false, useRefreshTokens: false };
        _authStatus = this._localStorageService.get('auth');
        if (!_authStatus)
            this._router.navigate(['/login']);
        else if ((!_authStatus.userName || 0 === _authStatus.userName.length)) {
            this._router.navigate(['/login']);
        }
        else {
            this._router.navigate(['/home']);
        }
    };
    LoginComponent.prototype.clicked = function () {
        var _this = this;
        this._loadingSvc.setValue(true);
        this._AuthService.login(this._userName, this._password, this._userefreshToken).subscribe((function (res) {
            _this.handleSuccessful(res);
            _this._loadingSvc.setValue(false);
        }), (function (error) { _this.handleFailed(error); }), (function () { return _this.handleComplete(); }));
    };
    LoginComponent.prototype.handleSuccessful = function (res) {
        this._successful = true;
        window.location.reload();
    };
    LoginComponent.prototype.handleFailed = function (error) {
        this._successful = false;
        this._msg = error;
    };
    LoginComponent.prototype.handleComplete = function () {
    };
    LoginComponent.prototype.authExternalProvider = function (provider) {
        var redirectUri = location.protocol + '//' + location.host + '/authcomplete';
        var externalProviderUrl = this._config.Server + "api/Account/ExternalLogin?provider=" + provider
            + "&response_type=token&client_id=" + this._config.clientID
            + "&redirect_uri=" + redirectUri;
        window.location.href = externalProviderUrl;
    };
    LoginComponent.prototype.parseQueryString = function (queryString) {
        var data = {}, pairs, pair, separatorIndex, escapedKey, escapedValue, key, value;
        if (queryString === null) {
            return data;
        }
        if (queryString.indexOf("amp;") > 0)
            pairs = queryString.split("&amp;");
        else
            pairs = queryString.split("&");
        for (var i = 0; i < pairs.length; i++) {
            pair = pairs[i];
            separatorIndex = pair.indexOf("=");
            if (separatorIndex === -1) {
                escapedKey = pair;
                escapedValue = null;
            }
            else {
                escapedKey = pair.substr(0, separatorIndex);
                escapedValue = pair.substr(separatorIndex + 1);
            }
            key = decodeURIComponent(escapedKey);
            value = decodeURIComponent(escapedValue);
            data[key] = value;
        }
        return data;
    };
    LoginComponent.prototype.parseFragmentsAndRedirect = function () {
        var _this = this;
        this._fragments = this.parseQueryString(this._fragments_string);
        if (this._fragments.haslocalaccount == 'False') {
            this._AuthService._externalAuthData = {
                provider: this._fragments.provider,
                userName: this._fragments.external_user_name,
                externalAccessToken: this._fragments.external_access_token
            };
            this._router.navigate(['/associate']);
        }
        else {
            var externalData = { provider: this._fragments.provider, externalAccessToken: this._fragments.external_access_token };
            this._AuthService.ObtainLocalAccessToken(externalData).subscribe((function (res) {
                window.location.href = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/home';
            }), (function (error) { _this.handleFailed(error); }));
        }
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    Component({
        selector: 'login',
        templateUrl: 'login.component.html',
    }),
    __metadata("design:paramtypes", [AuthService,
        Router,
        Configuration,
        LocalStorageService,
        LoadingAnimateService])
], LoginComponent);
export { LoginComponent };
//# sourceMappingURL=login.component.js.map