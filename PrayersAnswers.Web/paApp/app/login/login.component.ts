﻿import { Component, OnInit, NgModule, Input } from '@angular/core';
import { Router } from '@angular/router'
import { LocalStorageService } from './../services/localstorage.service';
import { AuthService } from './../services/authService';
import { Configuration } from './../app.constants';
import { IAuthStatus } from './../services/authStatus';
import { Subscription } from 'rxjs';
import { LoadingAnimateService } from 'ng2-loading-animate';




@Component({
    selector: 'login',
    templateUrl: 'login.component.html',

})

export class LoginComponent implements OnInit {

    private _userName: string;
    private _password: string;
    private _userefreshToken: boolean;
    private _msg: string;
    private _successful: boolean;
    private _fragments: any = {};
    private _fragments_string: string = "";
    


    constructor(private _AuthService: AuthService,
        private _router: Router,
        private _config: Configuration,
        private _localStorageService: LocalStorageService,
        private _loadingSvc: LoadingAnimateService
    ) {

    }

    ngOnInit() {
        this._msg = "";

        let _authStatus = { userName: "", isAuth: false, useRefreshTokens: false };
        _authStatus = this._localStorageService.get('auth');

        if (!_authStatus)
            this._router.navigate(['/login']);

        else if ((!_authStatus.userName || 0 === _authStatus.userName.length)) {
            this._router.navigate(['/login']);
        }
        else
        {
            this._router.navigate(['/home']);
        }
        

    }

    // button for login
    clicked() {

        this._loadingSvc.setValue(true);
        this._AuthService.login(this._userName, this._password, this._userefreshToken).subscribe(
            ((res) => {
                this.handleSuccessful(res);
                this._loadingSvc.setValue(false);
            })
            , ((error) => { this.handleFailed(error) })
            , (() => this.handleComplete())
        )

    }

    handleSuccessful(res:any) {
        this._successful = true;
        //this._router.navigate(['/home']);
        window.location.reload();
        
    }

    handleFailed(error: any) {
        this._successful = false;
        this._msg = error;

    }

    handleComplete() {

    }

    // button for external login

    authExternalProvider(provider: string) {
        var redirectUri = location.protocol + '//' + location.host + '/authcomplete';

        var externalProviderUrl = this._config.Server + "api/Account/ExternalLogin?provider=" + provider
            + "&response_type=token&client_id=" + this._config.clientID
            + "&redirect_uri=" + redirectUri;

        window.location.href = externalProviderUrl;
        //var oauthWindow = window.open(externalProviderUrl, "Authenticate Account", "location=0,status=0,width=600,height=750");



        //oauthWindow.addEventListener("unload", (() => {
        //    this._fragments_string = oauthWindow.document.getElementById('fragments').innerHTML;

        //    this.parseFragmentsAndRedirect();
        //}));

        //oauthWindow.onunload = (() => {
        //    this._fragments_string = oauthWindow.document.getElementById('fragments').innerHTML;
            
        //    this.parseFragmentsAndRedirect();
        //});
    }

    //valueReturnTest() {
    //    var redirectUri = location.protocol + '//' + location.host + '/authcomplete' + '#user_id=aaa&password=bbb';
    //    var oauthWindow = window.open(redirectUri, "Authenticate Account", "location=0,status=0,width=600,height=750");
    //    var newValue = "";





    //    oauthWindow.onunload = (() => {
    //        //this._fragments_string = oauthWindow.document.getElementById('fragments').innerHTML;
    //        this._fragments_string = this._localStorageService.get('fragments');
    //        this.parseFragmentsAndRedirect();
    //    });


    //}


    parseQueryString(queryString:any) {
    var data:any = {},
        pairs, pair, separatorIndex, escapedKey, escapedValue, key, value;

    if (queryString === null) {
        return data;
    }

    if (queryString.indexOf("amp;") > 0)
        pairs = queryString.split("&amp;");
    else
        pairs = queryString.split("&");

    for (var i = 0; i < pairs.length; i++) {
        pair = pairs[i];
        separatorIndex = pair.indexOf("=");

        if (separatorIndex === -1) {
            escapedKey = pair;
            escapedValue = null;
        } else {
            escapedKey = pair.substr(0, separatorIndex);
            escapedValue = pair.substr(separatorIndex + 1);
        }

        key = decodeURIComponent(escapedKey);
        value = decodeURIComponent(escapedValue);

        data[key] = value;
    }

    return data;
}


    parseFragmentsAndRedirect()
    {
        this._fragments = this.parseQueryString(this._fragments_string);

        if (this._fragments.haslocalaccount == 'False') {

            this._AuthService._externalAuthData = {
                provider: this._fragments.provider,
                userName: this._fragments.external_user_name,
                externalAccessToken: this._fragments.external_access_token
            };

            this._router.navigate(['/associate']);

        }
        else {
            //Obtain access token and redirect to home
            var externalData = { provider: this._fragments.provider, externalAccessToken: this._fragments.external_access_token };
            this._AuthService.ObtainLocalAccessToken(externalData).subscribe(
                ((res) => {
                    //this._router.navigate(['/home']);
                    window.location.href = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '')+ '/home'
                })
                , ((error) => { this.handleFailed(error) })
               )
            
        }
        

    }
  

}