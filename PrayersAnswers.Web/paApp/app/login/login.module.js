var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { LoadingAnimateModule } from 'ng2-loading-animate';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
var LoginModule = (function () {
    function LoginModule() {
    }
    return LoginModule;
}());
LoginModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            BrowserModule,
            FormsModule,
            BrowserAnimationsModule,
            LoadingAnimateModule.forRoot()
        ],
        declarations: [
            LoginComponent
        ],
        exports: [
            LoginComponent
        ]
    })
], LoginModule);
export { LoginModule };
//# sourceMappingURL=login.module.js.map