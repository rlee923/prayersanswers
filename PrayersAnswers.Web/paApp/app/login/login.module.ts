﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { LoadingAnimateModule, LoadingAnimateService } from 'ng2-loading-animate'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule,
        LoadingAnimateModule.forRoot()
    ],

    declarations: [
        LoginComponent
    ],
    exports: [
        LoginComponent
    ]

})

export class LoginModule { }