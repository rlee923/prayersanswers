var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { DataService } from './../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CheckAuthService } from './../services/checkauth.service';
var PrayerDetailComponent = (function () {
    function PrayerDetailComponent(_route, _dataService, _router, _checkAuthService, _localStorageService) {
        this._route = _route;
        this._dataService = _dataService;
        this._router = _router;
        this._checkAuthService = _checkAuthService;
        this._localStorageService = _localStorageService;
        this._answerExist = false;
    }
    PrayerDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this._route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.loadPrayerData();
        });
    };
    PrayerDetailComponent.prototype.loadPrayerData = function () {
        var _this = this;
        this._dataService.getPrayerWithAnswer(this.id)
            .subscribe((function (res) {
            _this._prayer = res.prayer;
            _this._title = _this._prayer.title;
            _this._notes = _this._prayer.notes;
            _this._answerExist = !!res.answer;
            _this._answer = res.answer;
        }), (function (error) { _this.handleFailed(error); }));
    };
    PrayerDetailComponent.prototype.editPrayerData = function () {
        var _this = this;
        var prayer = {
            id: this._prayer.id,
            userId: this._localStorageService.get('auth').userName,
            title: this._title,
            notes: this._notes
        };
        this._dataService.editPrayer(prayer)
            .subscribe((function (res) {
            _this._prayer = res;
            _this._title = _this._prayer.title;
            _this._notes = _this._prayer.notes;
            _this._msg = "Details saved, redirecting in 5 seconds";
            setTimeout(function (router) {
                _this._router.navigate(['/prayers']);
            }, 5000);
        }), (function (error) { _this.handleFailed(error); }));
    };
    PrayerDetailComponent.prototype.deletePrayerData = function () {
        var _this = this;
        if (confirm("Are you sure to delete?")) {
            this._dataService.deletePrayer(this._prayer.id)
                .subscribe((function (res) {
                _this._prayer = res;
                _this._title = _this._prayer.title;
                _this._notes = _this._prayer.notes;
                _this._msg = "Prayer Data deleted, redirecting in 5 seconds";
                setTimeout(function (router) {
                    _this._router.navigate(['/prayers']);
                }, 5000);
            }), (function (error) { _this.handleFailed(error); }));
        }
    };
    PrayerDetailComponent.prototype.addAnswer = function () {
        var _this = this;
        var prayer = {
            id: this._prayer.id,
            userId: this._localStorageService.get('auth').userName,
            title: this._title,
            notes: this._notes
        };
        this._dataService.editPrayer(prayer)
            .subscribe((function (res) {
            _this._router.navigate(['/answer-new', _this._prayer.id]);
        }), (function (error) { _this.handleFailed(error); }));
    };
    PrayerDetailComponent.prototype.handleFailed = function (error) {
        this._msg = error;
        this._checkAuthService.handleError(error, this._router);
    };
    return PrayerDetailComponent;
}());
PrayerDetailComponent = __decorate([
    Component({
        selector: 'prayer-detail',
        templateUrl: 'prayer.detail.component.html',
    }),
    __metadata("design:paramtypes", [ActivatedRoute,
        DataService,
        Router,
        CheckAuthService,
        LocalStorageService])
], PrayerDetailComponent);
export { PrayerDetailComponent };
//# sourceMappingURL=prayer.detail.component.js.map