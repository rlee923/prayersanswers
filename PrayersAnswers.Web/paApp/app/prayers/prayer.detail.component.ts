﻿import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { DataService } from './../services/data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { IAuthStatus } from './../services/authStatus';
import { CheckAuthService } from './../services/checkauth.service';


@Component({
    selector: 'prayer-detail',
    templateUrl: 'prayer.detail.component.html',


})

export class PrayerDetailComponent implements OnInit {

    private sub: any;
    id: number;
    private _prayer: any;
    private _title: string;
    private _notes: string;
    private _msg: string;
    private _answer: any;
    private _answerExist: boolean = false;

    constructor(private _route: ActivatedRoute,
        private _dataService: DataService,
        private _router: Router,
        private _checkAuthService: CheckAuthService,
        private _localStorageService: LocalStorageService
    ) {

    }

    ngOnInit() {
        this.sub = this._route.params.subscribe((params) => {
            this.id = +params['id']; // (+) converts string 'id' to a number

            // load data here

            this.loadPrayerData();
        });

    }

    loadPrayerData() {
        this._dataService.getPrayerWithAnswer(this.id)
            .subscribe(
            ((res) => {
                this._prayer = res.prayer;
                this._title = this._prayer.title;
                this._notes = this._prayer.notes;
                this._answerExist = !!res.answer;
                this._answer = res.answer;
            })
            , ((error) => { this.handleFailed(error) })
            )

    }

    editPrayerData() {
        let prayer = {
            id: this._prayer.id,
            userId: this._localStorageService.get('auth').userName,
            title: this._title,
            notes: this._notes
        }

        this._dataService.editPrayer(prayer)
            .subscribe(
            ((res) => {
                this._prayer = res;
                this._title = this._prayer.title;
                this._notes = this._prayer.notes;

                this._msg = "Details saved, redirecting in 5 seconds";
                setTimeout((router: Router) => {
                    this._router.navigate(['/prayers']);
                }, 5000)


            })
            , ((error) => { this.handleFailed(error) })
            )
    }

    deletePrayerData() {
        if (confirm("Are you sure to delete?")) {

            this._dataService.deletePrayer(this._prayer.id)
                .subscribe(
                ((res) => {
                    this._prayer = res;
                    this._title = this._prayer.title;
                    this._notes = this._prayer.notes;

                    this._msg = "Prayer Data deleted, redirecting in 5 seconds";
                    setTimeout((router: Router) => {
                        this._router.navigate(['/prayers']);
                    }, 5000)


                })
                , ((error) => { this.handleFailed(error) })
                )
        }
    }


    addAnswer() {
        let prayer = {
            id: this._prayer.id,
            userId: this._localStorageService.get('auth').userName,
            title: this._title,
            notes: this._notes
        }

        this._dataService.editPrayer(prayer)
            .subscribe(
            ((res) => {

                this._router.navigate(['/answer-new', this._prayer.id]);
                


            })
            , ((error) => { this.handleFailed(error) })
            )


    }
        


    handleFailed(error: any) {

        this._msg = error;
        this._checkAuthService.handleError(error, this._router);

    }



}