﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PrayerDetailComponent } from './prayer.detail.component';
import { RouterModule } from '@angular/router';



@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,
        RouterModule

        
    ],

    declarations: [
        PrayerDetailComponent
    ],
    exports: [
        PrayerDetailComponent
    ]

})

export class PrayerDetailModule { }