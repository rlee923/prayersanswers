var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { DataService } from './../services/data.service';
import { Router } from '@angular/router';
import { CheckAuthService } from './../services/checkauth.service';
var PrayerNewComponent = (function () {
    function PrayerNewComponent(_dataService, _router, _checkAuthService, _localStorageService) {
        this._dataService = _dataService;
        this._router = _router;
        this._checkAuthService = _checkAuthService;
        this._localStorageService = _localStorageService;
    }
    PrayerNewComponent.prototype.ngOnInit = function () {
        this._checkAuthService.handleError("", this._router);
    };
    PrayerNewComponent.prototype.addPrayerData = function () {
        var _this = this;
        var prayer = {
            userId: this._localStorageService.get('auth').userName,
            title: this._title,
            notes: this._notes
        };
        this._dataService.addPrayer(prayer)
            .subscribe((function (res) {
            _this._prayer = res;
            _this._title = _this._prayer.title;
            _this._notes = _this._prayer.notes;
            _this._msg = "Prayer entry created, redirecting in 5 seconds";
            setTimeout(function (router) {
                _this._router.navigate(['/prayers']);
            }, 5000);
        }), (function (error) { _this.handleFailed(error); }));
    };
    PrayerNewComponent.prototype.handleFailed = function (error) {
        this._msg = error;
        this._checkAuthService.handleError(error, this._router);
    };
    return PrayerNewComponent;
}());
PrayerNewComponent = __decorate([
    Component({
        selector: 'prayer-new',
        templateUrl: 'prayer.new.component.html',
    }),
    __metadata("design:paramtypes", [DataService,
        Router,
        CheckAuthService,
        LocalStorageService])
], PrayerNewComponent);
export { PrayerNewComponent };
//# sourceMappingURL=prayer.new.component.js.map