﻿import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { DataService } from './../services/data.service';
import { Router,  Params } from '@angular/router';
import { IAuthStatus } from './../services/authStatus';
import { CheckAuthService } from './../services/checkauth.service';


@Component({
    selector: 'prayer-new',
    templateUrl: 'prayer.new.component.html',


})

export class PrayerNewComponent implements OnInit {
    private _prayer: any;
    private _title: string;
    private _notes: string;
    private _msg: string;

    constructor(
        private _dataService: DataService,
        private _router: Router,
        private _checkAuthService: CheckAuthService,
        private _localStorageService: LocalStorageService
    ) {

    }

    ngOnInit() {
        this._checkAuthService.handleError("", this._router);
    }

    addPrayerData()
    {
        let prayer = {
            
            userId: this._localStorageService.get('auth').userName,
            title: this._title,
            notes: this._notes
        }

        this._dataService.addPrayer(prayer)
            .subscribe(
            ((res) => {
                this._prayer = res;
                this._title = this._prayer.title;
                this._notes = this._prayer.notes;

                this._msg = "Prayer entry created, redirecting in 5 seconds";
                setTimeout((router: Router) => {
                    this._router.navigate(['/prayers']);
                }, 5000)


            })
            , ((error) => { this.handleFailed(error) })
            )
    }

    handleFailed(error: any) {

        this._msg = error;
        this._checkAuthService.handleError(error, this._router);

    }
}