﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PrayerNewComponent } from './prayer.new.component';


@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule


    ],

    declarations: [
        PrayerNewComponent
    ],
    exports: [
        PrayerNewComponent
    ]

})

export class PrayerNewModule { }