﻿import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { DataService } from './../services/data.service';
import { Router } from '@angular/router';
import { IAuthStatus } from './../services/authStatus';
import { PagerService } from './../services/page.service';
import { CheckAuthService } from './../services/checkauth.service';
// AoT compilation doesn't support 'require'.


@Component({
    selector: 'prayers',
    templateUrl: 'prayers.component.html',
    providers: [PagerService]
    
})

export class PrayersComponent implements OnInit {

    private _authStatus: IAuthStatus;
    private _prayers:any[];
    private _msg: string = "";
    private _pager: any = {};

    constructor(private _localStorageService: LocalStorageService,
        private _router: Router,
        private _dataService: DataService,
        private _pagerService: PagerService,
        private _checkAuthService: CheckAuthService) {
    }


    ngOnInit() {
        this._checkAuthService.handleError("", this._router)

        //this.getPrayers();

        this.setPage(1);
    }


    getPrayers() {

        // get total prayers count


        // call data service to get prayers
        this._dataService.getPrayersbyPage().subscribe(
            ((res) => {
                this._prayers = res;
            })
            , ((error) => { this.handleFailed(error) })
            
        )

    }

    getTotalItemCount(): number
    {
        let totalItemCount: number = 0;
        this._dataService.getPrayersCount().subscribe(
            ((res) => {
                totalItemCount = res;
            })
            , ((error) => { this.handleFailed(error) })

        )
        return totalItemCount;
    }

    getPrayersByPage(pageNum: number) {
        let totalItemCount: number = 0;
        this._dataService.GetPrayersWithAnswersByPage(pageNum).subscribe(
            ((res) => {
                this._prayers = res;
            })
            , ((error) => { this.handleFailed(error) })

        )
        return totalItemCount;
    }

    setPage(page: number) {
        if (page < 1 || page > this._pager.totalPages) {
            return;
        }

        let totalItemCount: number = 0;

        this._dataService.getPrayersCount().subscribe(
            ((res) => {
                totalItemCount = res;
                this._pager = this._pagerService.getPager(totalItemCount, page);
                this.getPrayersByPage(page);
            })
            , ((error) => { this.handleFailed(error) })

        )
        

        // get pager object from service
        

        // get current page of items
        
    }


    addNewPrayer() {
        // redirect to new prayer page
        this._router.navigate(['/prayer-new']);
        
    }
    

    handleFailed(error: any) {
        
        this._msg = error;
        this._checkAuthService.handleError(error, this._router);
    }
}