﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PrayersComponent } from './prayers.component';
import { RouterModule } from '@angular/router';


@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,
        RouterModule
        
    ],

    declarations: [
        PrayersComponent
    ],
    exports: [
        PrayersComponent
    ]
    
})

export class PrayersModule { }