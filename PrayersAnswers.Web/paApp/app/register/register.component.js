var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AuthService } from './../services/authService';
import { Router } from '@angular/router';
var RegisterComponent = (function () {
    function RegisterComponent(_AuthService, _router) {
        this._AuthService = _AuthService;
        this._router = _router;
        this._msg = "";
        this._success = false;
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent.prototype.clicked = function () {
        var _this = this;
        this._success = false;
        if (!this._password) {
            this._msg = "please check password & confirm password";
        }
        else {
            if (this._password != this._confirmPassword) {
                this._msg = "please check password & confirm password";
            }
        }
        var result;
        this._AuthService
            .register(this._username, this._email, this._password, this._confirmPassword)
            .subscribe(function (res) { _this.successAndRedirect(); }, function (error) { _this.failedToRegister(error); });
    };
    RegisterComponent.prototype.successAndRedirect = function () {
        var _this = this;
        this._success = true;
        this._msg = "registation complete, redirecting to login page";
        setTimeout(function (router) {
            _this._router.navigate(['./login']);
        }, 5000);
    };
    RegisterComponent.prototype.failedToRegister = function (error) {
        this._success = false;
        this._msg = error;
    };
    return RegisterComponent;
}());
RegisterComponent = __decorate([
    Component({
        selector: 'register',
        templateUrl: 'register.component.html',
    }),
    __metadata("design:paramtypes", [AuthService, Router])
], RegisterComponent);
export { RegisterComponent };
//# sourceMappingURL=register.component.js.map