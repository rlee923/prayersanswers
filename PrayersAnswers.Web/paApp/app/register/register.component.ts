﻿import { Component, OnInit, NgModule} from '@angular/core';
import { LocalStorageService } from './../services/localstorage.service';
import { AuthService } from './../services/authService';
import { Router } from '@angular/router';



// AoT compilation doesn't support 'require'.


@Component({
    selector: 'register',
    templateUrl: 'register.component.html',

})

export class RegisterComponent implements OnInit {

    private _username: string;
    private _email: string;
    private _password: string;
    private _confirmPassword: string;
    private _msg: string = "";
    private _success: boolean = false;


    constructor(private _AuthService: AuthService, private _router: Router) {

    }

    ngOnInit() {
            
    }

    clicked()
    {
        this._success = false;

        // compare password
        if (!this._password) {
            this._msg = "please check password & confirm password"
        }
        else {
            if (this._password != this._confirmPassword)
            {
                this._msg = "please check password & confirm password"
            }

        }

        let result: any;
        // call authservice register

        this._AuthService
            .register(this._username, this._email, this._password, this._confirmPassword)
            .subscribe((res) => { this.successAndRedirect() }
            , (error) => { this.failedToRegister(error) }
            )

        
            
        

        // if successful redirect to login after 5 secs

        
    }

    successAndRedirect()
    {
        this._success = true;
        this._msg = "registation complete, redirecting to login page"
        setTimeout((router: Router) => {
            this._router.navigate(['./login']);
        }, 5000)
    }

    failedToRegister(error:any)
    {
        this._success = false;
        this._msg = error;
    }

    

}