﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register.component';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,
    ],

    declarations: [
        RegisterComponent
    ],
    exports: [
        RegisterComponent
    ]

})

export class RegisterModule { }