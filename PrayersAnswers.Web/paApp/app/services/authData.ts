﻿
export interface IExternalAuthData {
    provider: string,
    userName: string,
    externalAccessToken: string
}

