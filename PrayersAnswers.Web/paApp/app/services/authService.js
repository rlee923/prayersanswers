var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { LocalStorageService } from './localstorage.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Configuration } from './../app.constants';
var AuthService = (function () {
    function AuthService(_configuration, _http, _localStorageService) {
        this._configuration = _configuration;
        this._http = _http;
        this._localStorageService = _localStorageService;
        this._baseUri = _configuration.Server;
        this._authStatus = { userName: "", isAuth: false, useRefreshTokens: false };
    }
    AuthService.prototype.handleError = function (error) {
        console.error(error);
        var msg = '';
        if (error.json().error_description)
            msg = error.json().error_description;
        else {
            var errors = [];
            var modelStates = JSON.parse(error._body).modelState;
            for (var key in modelStates) {
                errors.push(modelStates[key]);
            }
            msg = errors.join(' ');
        }
        return Observable.throw(msg || 'Server error');
    };
    AuthService.prototype.parseErrors = function (response) {
        var errors = [];
        for (var _i = 0, _a = response.modelState; _i < _a.length; _i++) {
            var key = _a[_i];
            errors.push(response.modelState[key]);
        }
        return errors.join(' ');
    };
    AuthService.prototype.logOut = function () {
        this._authStatus.isAuth = false;
        this._authStatus.userName = "";
        this._authStatus.useRefreshTokens = false;
        this._localStorageService.set('auth', this._authStatus);
    };
    AuthService.prototype.register = function (userName, emailAddress, password, confirmPassword) {
        this.logOut();
        var headers = new Headers({ 'Content-Type': 'application/json' });
        var options = new RequestOptions({ headers: headers });
        return this._http.post(this._baseUri + 'api/account/register', JSON.stringify({ username: userName, emailAddress: emailAddress, password: password, confirmPassword: confirmPassword }), options)
            .map(function (res) {
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    AuthService.prototype.login = function (userName, password, useRefreshToken) {
        var _this = this;
        var data = "grant_type=password&username=" + userName + "&password=" + password;
        var headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded;charset:UTF-8' });
        var options = new RequestOptions({ headers: headers });
        return this._http.post(this._baseUri + 'token', data, options)
            .map(function (res) {
            _this._authStatus = { userName: res.json().userName, isAuth: true, useRefreshTokens: useRefreshToken };
            _this._localStorageService.set('auth', _this._authStatus);
            _this._localStorageService.set('authData', { token: res.json().access_token, userName: userName, refreshToken: "", useRefreshTokens: false });
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    AuthService.prototype.ObtainLocalAccessToken = function (registerExternalData) {
        var _this = this;
        var headers = new Headers({ 'Content-Type': 'application/json' });
        var options = new RequestOptions({ headers: headers });
        return this._http.get(this._baseUri + 'api/account/ObtainLocalAccessToken?'
            + 'provider=' + registerExternalData.provider
            + '&externalAccessToken=' + registerExternalData.externalAccessToken, options)
            .map(function (res) {
            _this._localStorageService.set('authData', { token: res.json().access_token, userName: res.json().userName, refreshToken: "", useRefreshTokens: false });
            _this._authStatus = { userName: res.json().userName, isAuth: true, useRefreshTokens: false };
            _this._localStorageService.set('auth', _this._authStatus);
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    AuthService.prototype.registerExternal = function (registerExternalData) {
        var _this = this;
        var headers = new Headers({ 'Content-Type': 'application/json' });
        var options = new RequestOptions({ headers: headers });
        return this._http.post(this._baseUri + 'api/account/registerExternal', registerExternalData, options)
            .map(function (res) {
            _this._localStorageService.set('authData', { token: res.json().access_token, userName: res.json().userName, refreshToken: "", useRefreshTokens: false });
            _this._authStatus = { userName: res.json().userName, isAuth: true, useRefreshTokens: false };
            _this._localStorageService.set('auth', _this._authStatus);
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    AuthService.prototype.setAuthStatus = function () {
    };
    AuthService.prototype.refreshToken = function () {
    };
    return AuthService;
}());
AuthService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Configuration, Http, LocalStorageService])
], AuthService);
export { AuthService };
//# sourceMappingURL=authService.js.map