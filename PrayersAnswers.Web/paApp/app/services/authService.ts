﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { LocalStorageService } from './localstorage.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Configuration } from './../app.constants';
import { IAuthStatus } from './authStatus';
import { IExternalAuthData } from './authData';

@Injectable()
export class AuthService {

    private _baseUri: string;
    private _authStatus: IAuthStatus;
    public _externalAuthData: IExternalAuthData;

    constructor(private _configuration: Configuration, private _http: Http, private _localStorageService: LocalStorageService) {

        this._baseUri = _configuration.Server;
        this._authStatus = { userName: "", isAuth: false, useRefreshTokens: false };

    }





    private handleError(error: any) {

        console.error(error);
        let msg: string = '';

        if (error.json().error_description)
            msg = error.json().error_description;

        else {
            let errors = [];
            let modelStates = JSON.parse(error._body).modelState;
            for (let key in modelStates) {
                
                errors.push(modelStates[key]);
            }

            msg = errors.join(' ');
        }



        return Observable.throw(msg || 'Server error');
    }

    private parseErrors(response: any): string {
        let errors = [];
        for (let key of response.modelState) {
            errors.push(response.modelState[key]);
        }
        return errors.join(' ');
    }


    public logOut(): void {

        this._authStatus.isAuth = false;
        this._authStatus.userName = "";
        this._authStatus.useRefreshTokens = false;

        this._localStorageService.set('auth', this._authStatus);
    }

    public register(userName: string, emailAddress: string, password: string, confirmPassword: string): Observable<any> {

        this.logOut();

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this._http.post(this._baseUri + 'api/account/register'
            , JSON.stringify({ username: userName, emailAddress: emailAddress, password: password, confirmPassword: confirmPassword })
            , options)
            .map((res: Response) => {
                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);

    }

    public login(userName: string, password: string, useRefreshToken: boolean): Observable<any> {

        var data = "grant_type=password&username=" + userName + "&password=" + password;
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded;charset:UTF-8' });
        let options = new RequestOptions({ headers: headers });
        return this._http.post(this._baseUri + 'token', data, options)
            .map((res: Response) => {

                // save authData and authStatus into localservice
                this._authStatus = { userName: res.json().userName, isAuth: true, useRefreshTokens: useRefreshToken };
                this._localStorageService.set('auth', this._authStatus);
                this._localStorageService.set('authData', { token: res.json().access_token, userName: userName, refreshToken: "", useRefreshTokens: false });

                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);



        // save authentication into local variable


    }

    

    public ObtainLocalAccessToken(registerExternalData: any): Observable<any>  {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.get(this._baseUri + 'api/account/ObtainLocalAccessToken?'
            + 'provider=' + registerExternalData.provider
            + '&externalAccessToken=' + registerExternalData.externalAccessToken
            ,  options)
            .map((res: Response) => {

                // save authData and authStatus into localservice
                this._localStorageService.set('authData', { token: res.json().access_token, userName: res.json().userName, refreshToken: "", useRefreshTokens: false });
                this._authStatus = { userName: res.json().userName, isAuth: true, useRefreshTokens: false };
                this._localStorageService.set('auth', this._authStatus);


                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);
    }


    public registerExternal(registerExternalData: any): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post(this._baseUri + 'api/account/registerExternal', registerExternalData, options)
            .map((res: Response) => {

                // save authData and authStatus into localservice
                this._localStorageService.set('authData', { token: res.json().access_token, userName: res.json().userName, refreshToken: "", useRefreshTokens: false });
                this._authStatus = { userName: res.json().userName, isAuth: true, useRefreshTokens: false };
                this._localStorageService.set('auth', this._authStatus);


                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);
    }


    public setAuthStatus(): void {



    }

    public refreshToken(): void {

    }


}