﻿
export interface IAuthStatus {
    isAuth: boolean,
    userName: string,
    useRefreshTokens: boolean
}

