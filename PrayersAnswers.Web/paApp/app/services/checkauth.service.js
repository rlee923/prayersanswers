var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { LocalStorageService } from './localstorage.service';
var CheckAuthService = (function () {
    function CheckAuthService(_localStorageService) {
        this._localStorageService = _localStorageService;
    }
    CheckAuthService.prototype.handleError = function (error, router) {
        this._authStatus = { userName: "", isAuth: false, useRefreshTokens: false };
        this._authStatus = this._localStorageService.get('auth');
        if (!this._authStatus)
            window.location.href = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/login';
        else if ((!this._authStatus.userName || 0 === this._authStatus.userName.length)) {
            window.location.href = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/login';
        }
    };
    return CheckAuthService;
}());
CheckAuthService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [LocalStorageService])
], CheckAuthService);
export { CheckAuthService };
//# sourceMappingURL=checkauth.service.js.map