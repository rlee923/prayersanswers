﻿import { Injectable } from '@angular/core';
import { LocalStorageService } from './localstorage.service'
import { Router } from '@angular/router';

@Injectable()
export class CheckAuthService {

    private _authStatus: any;

    constructor(private _localStorageService: LocalStorageService)
    {

    }

    handleError(error: any, router: Router)
    {
        this._authStatus = { userName: "", isAuth: false, useRefreshTokens: false };
        this._authStatus = this._localStorageService.get('auth');

        if (!this._authStatus)
            //router.navigate(['/login']);
            
            window.location.href = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/login'

        else if ((!this._authStatus.userName || 0 === this._authStatus.userName.length)) {

            //router.navigate(['/login']);
            window.location.href = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/login'
        }
    }

    


}