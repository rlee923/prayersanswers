var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Configuration } from './../app.constants';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { LocalStorageService } from './localstorage.service';
import { HttpClient } from './httpClient.service';
var DataService = (function () {
    function DataService(_http, _localStorageService, _config) {
        this._http = _http;
        this._localStorageService = _localStorageService;
        this._config = _config;
        this._prayers = [];
        this.generateDummyData();
    }
    DataService.prototype.handleError = function (error) {
        console.error(error);
        var msg = '';
        if (error.json().error_description)
            msg = error.json().error_description;
        else {
            var errors = [];
            var modelStates = JSON.parse(error._body).modelState;
            for (var key in modelStates) {
                errors.push(modelStates[key]);
            }
            msg = errors.join(' ');
        }
        return Observable.throw(msg || 'Server error');
    };
    DataService.prototype.generateDummyData = function () {
        var i = 0;
        for (i = 0; i < 22; i++) {
            this._prayers.push({
                id: i, userId: "aaa", dateStarted: new Date(2017, i, 1), title: "new title" + i.toString(), notes: "notes" + i.toString()
            });
        }
        return this._prayers;
    };
    DataService.prototype.getPrayersbyPage = function (pageNum) {
        if (pageNum === void 0) { pageNum = 1; }
        return this._http.get(this._config.Server + 'api/prayer/GetPrayersByPage?' + 'pageNum=' + pageNum)
            .map(function (res) {
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    DataService.prototype.GetPrayersWithAnswersByPage = function (pageNum) {
        if (pageNum === void 0) { pageNum = 1; }
        return this._http.get(this._config.Server + 'api/prayer/GetPrayersWithAnswersByPage?' + 'pageNum=' + pageNum)
            .map(function (res) {
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    DataService.prototype.getPrayersCount = function () {
        return this._http.get(this._config.Server + 'api/prayer/GetPrayersCount')
            .map(function (res) {
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    DataService.prototype.getPrayer = function (id) {
        return this._http.get(this._config.Server + 'api/prayer/GetPrayer?' + 'id=' + id)
            .map(function (res) {
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    DataService.prototype.getPrayerWithAnswer = function (id) {
        return this._http.get(this._config.Server + 'api/prayer/GetPrayerWithAnswer?' + 'id=' + id)
            .map(function (res) {
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    DataService.prototype.editPrayer = function (prayer) {
        return this._http.put(this._config.Server + 'api/prayer/put', prayer)
            .map(function (res) {
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    DataService.prototype.addPrayer = function (prayer) {
        return this._http.post(this._config.Server + 'api/prayer/post', prayer)
            .map(function (res) {
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    DataService.prototype.deletePrayer = function (id) {
        return this._http.delete(this._config.Server + 'api/prayer/delete?prayerId=' + id.toString(), null)
            .map(function (res) {
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    DataService.prototype.throwUnAuthError = function () {
        return this._http.get(this._config.Server + 'api/prayer/throw401')
            .map(function (res) {
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    DataService.prototype.getAnswersByPage = function (pageNum) {
        if (pageNum === void 0) { pageNum = 1; }
        return this._http.get(this._config.Server + 'api/answer/GetAnswersByPage?' + 'pageNum=' + pageNum)
            .map(function (res) {
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    DataService.prototype.getAnswersCount = function () {
        return this._http.get(this._config.Server + 'api/answer/GetAnswersCount')
            .map(function (res) {
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    DataService.prototype.getAnswer = function (id) {
        return this._http.get(this._config.Server + 'api/answer/GetAnswer?' + 'id=' + id)
            .map(function (res) {
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    DataService.prototype.editAnswer = function (answer) {
        return this._http.put(this._config.Server + 'api/answer/put', answer)
            .map(function (res) {
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    DataService.prototype.addAnswer = function (answer) {
        return this._http.post(this._config.Server + 'api/answer/post', answer)
            .map(function (res) {
            var body;
            if (res.text()) {
                body = res.json();
            }
            return body || {};
        })
            .catch(this.handleError);
    };
    return DataService;
}());
DataService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [HttpClient, LocalStorageService, Configuration])
], DataService);
export { DataService };
//# sourceMappingURL=data.service.js.map