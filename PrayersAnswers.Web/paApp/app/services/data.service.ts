﻿import { httpInterceptService } from './httpIntercept.service';
import {  Response } from '@angular/http';
import { Configuration } from './../app.constants';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { LocalStorageService } from './localstorage.service';
import { HttpClient } from './httpClient.service';


@Injectable()
export class DataService
{
    private _prayers:any[] = [];
    constructor(private _http: HttpClient, private _localStorageService: LocalStorageService, private _config:Configuration) {
        this.generateDummyData();
    }

    private handleError(error: any) {

        console.error(error);
        let msg: string = '';

        if (error.json().error_description)
            msg = error.json().error_description;

        else {
            let errors = [];
            let modelStates = JSON.parse(error._body).modelState;
            for (let key in modelStates) {

                errors.push(modelStates[key]);
            }

            msg = errors.join(' ');
        }



        return Observable.throw(msg || 'Server error');
    }

    // prayers

    generateDummyData() {
        let i = 0;
        for (i = 0; i < 22; i++){
            this._prayers.push({
                id: i, userId: "aaa", dateStarted: new Date(2017, i, 1), title: "new title" + i.toString(), notes: "notes" + i.toString()
            });
        }

        return this._prayers;
    }


    getPrayersbyPage(pageNum: number=1): Observable<any> {

        
        //return Observable.of(this._prayers);

        return this._http.get(this._config.Server + 'api/prayer/GetPrayersByPage?' + 'pageNum=' + pageNum)
            .map((res: Response) => {
                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);

    }

    GetPrayersWithAnswersByPage(pageNum: number = 1): Observable<any> {


        //return Observable.of(this._prayers);

        return this._http.get(this._config.Server + 'api/prayer/GetPrayersWithAnswersByPage?' + 'pageNum=' + pageNum)
            .map((res: Response) => {
                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);

    }

    getPrayersCount(): Observable<any> {
        return this._http.get(this._config.Server + 'api/prayer/GetPrayersCount')
            .map((res: Response) => {
                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);
    }

    getPrayer(id:number): Observable<any> {


        return this._http.get(this._config.Server + 'api/prayer/GetPrayer?' + 'id=' + id)
            .map((res: Response) => {
                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);
        
    }

    getPrayerWithAnswer(id: number): Observable<any> {


        return this._http.get(this._config.Server + 'api/prayer/GetPrayerWithAnswer?' + 'id=' + id)
            .map((res: Response) => {
                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);

    }


    editPrayer(prayer: any): Observable<any> {
        return this._http.put(this._config.Server + 'api/prayer/put', prayer)
            .map((res: Response) => {
                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);
    }

    addPrayer(prayer: any): Observable<any> {
        return this._http.post(this._config.Server + 'api/prayer/post', prayer)
            .map((res: Response) => {
                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);
    }

    deletePrayer(id: number): Observable<any> {
        return this._http.delete(this._config.Server + 'api/prayer/delete?prayerId=' + id.toString(), null)
            .map((res: Response) => {
                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);
    }



    throwUnAuthError()
    {

        return this._http.get(this._config.Server + 'api/prayer/throw401')
            .map((res: Response) => {
                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);
    }


    // answers


    getAnswersByPage(pageNum: number = 1): Observable<any> {


        //return Observable.of(this._prayers);

        return this._http.get(this._config.Server + 'api/answer/GetAnswersByPage?' + 'pageNum=' + pageNum)
            .map((res: Response) => {
                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);

    }


   

    getAnswersCount(): Observable<any> {
        return this._http.get(this._config.Server + 'api/answer/GetAnswersCount')
            .map((res: Response) => {
                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);
    }

    getAnswer(id: number): Observable<any> {


        return this._http.get(this._config.Server + 'api/answer/GetAnswer?' + 'id=' + id)
            .map((res: Response) => {
                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);

    }


    editAnswer(answer: any): Observable<any> {
        return this._http.put(this._config.Server + 'api/answer/put', answer)
            .map((res: Response) => {
                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);
    }

    addAnswer(answer: any): Observable<any> {
        return this._http.post(this._config.Server + 'api/answer/post', answer)
            .map((res: Response) => {
                let body;
                if (res.text()) {
                    body = res.json();
                }

                return body || {};

            })
            .catch(this.handleError);
    }

}