var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { LocalStorageService } from './localstorage.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
var HttpClient = (function () {
    function HttpClient(http, _localStorageService) {
        this.http = http;
        this._localStorageService = _localStorageService;
    }
    HttpClient.prototype.createAuthorizationHeader = function (headers) {
        this._authData = {};
        this._authStatus = this._localStorageService.get('auth');
        if (!this._authStatus)
            return;
        if ((this._authStatus.userName && (0 != this._authStatus.userName.length))) {
            this._authData = this._localStorageService.get('authData');
            headers.append('Authorization', 'Bearer ' + this._authData.token);
            headers.append('Content-Type', 'application/json');
        }
    };
    HttpClient.prototype.get = function (url) {
        var _this = this;
        var headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.get(url, {
            headers: headers
        }).catch(function (err) { return _this.handleError(err); });
    };
    HttpClient.prototype.post = function (url, data) {
        var _this = this;
        var headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.post(url, data, {
            headers: headers
        }).catch(function (err) { return _this.handleError(err); });
    };
    HttpClient.prototype.put = function (url, data) {
        var _this = this;
        var headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.put(url, data, {
            headers: headers
        }).catch(function (err) { return _this.handleError(err); });
    };
    HttpClient.prototype.delete = function (url, data) {
        var _this = this;
        var headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.delete(url, {
            headers: headers
        }).catch(function (err) { return _this.handleError(err); });
    };
    HttpClient.prototype.handleError = function (err) {
        if (err.status == 401) {
            this._localStorageService.remove('auth');
            this._localStorageService.remove('authData');
        }
        return Observable.throw('Server error');
    };
    return HttpClient;
}());
HttpClient = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http, LocalStorageService])
], HttpClient);
export { HttpClient };
//# sourceMappingURL=httpClient.service.js.map