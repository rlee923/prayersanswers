﻿import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { LocalStorageService } from './localstorage.service';
import { IAuthStatus } from './authStatus';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class HttpClient {

    private _authData: any;

    private _authStatus: IAuthStatus;

    constructor(private http: Http, private _localStorageService: LocalStorageService) {


    }



    createAuthorizationHeader(headers: Headers) {
        this._authData = {};
        this._authStatus = this._localStorageService.get('auth');
        if (!this._authStatus)
            return;


        if ((this._authStatus.userName && (0 != this._authStatus.userName.length))) {
            this._authData = this._localStorageService.get('authData');
            headers.append('Authorization', 'Bearer ' + this._authData.token);
            headers.append('Content-Type', 'application/json');
        }
    }

    get(url:string) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.get(url, {
            headers: headers
        }).catch((err) => this.handleError(err));
    }

    post(url:string, data:any) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.post(url, data, {
            headers: headers
        }).catch((err) => this.handleError(err));
            
    }

    put(url: string, data: any) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.put(url, data, {
            headers: headers
        }).catch((err) => this.handleError(err));

    }

    delete(url: string, data: any) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.delete(url,  {
            headers: headers
        }).catch((err) => this.handleError(err));

    }



    private handleError(err:any) {
        if (err.status == 401)
        {
            
            this._localStorageService.remove('auth');
            this._localStorageService.remove('authData');
        }
        return Observable.throw('Server error');
    }
}