var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { LocalStorageService } from './localstorage.service';
import { RequestOptions, ConnectionBackend, Http, Headers } from "@angular/http";
import { Router } from '@angular/router';
var httpInterceptService = (function (_super) {
    __extends(httpInterceptService, _super);
    function httpInterceptService(backend, defaultOptions, _router, _localStorageService) {
        var _this = _super.call(this, backend, defaultOptions) || this;
        _this._router = _router;
        _this._localStorageService = _localStorageService;
        _this.headers = new Headers({ 'Something': 'Something' });
        _this.options1 = new RequestOptions({ headers: _this.headers });
        return _this;
    }
    httpInterceptService.prototype.handleError = function (error) {
        console.error(error);
        var msg = '';
        if (error.json().error_description)
            msg = error.json().error_description;
        else {
            var errors = [];
            var modelStates = JSON.parse(error._body).modelState;
            for (var key in modelStates) {
                errors.push(modelStates[key]);
            }
            msg = errors.join(' ');
        }
        return Observable.throw(msg || 'Server error');
    };
    httpInterceptService.prototype.getRequestOptionArgs = function (options) {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }
        this._authData = {};
        this._authStatus = this._localStorageService.get('auth');
        if ((!this._authStatus.userName || 0 === this._authStatus.userName.length)) {
            this._authData = this._localStorageService.get('authData');
            options.headers.append('Authorization', 'Bearer ' + this._authData.token);
            options.headers.append('Content-Type', 'application/json');
        }
        return options;
    };
    httpInterceptService.prototype.get = function (url, options) {
        return this.intercept(_super.prototype.post.call(this, url, this.getRequestOptionArgs(options))).catch(this.handleError);
    };
    httpInterceptService.prototype.post = function (url, body, options) {
        return this.intercept(_super.prototype.post.call(this, url, body, this.getRequestOptionArgs(options)));
    };
    httpInterceptService.prototype.put = function (url, body, options) {
        return this.intercept(_super.prototype.put.call(this, url, body, this.getRequestOptionArgs(options)));
    };
    httpInterceptService.prototype.delete = function (url, options) {
        return this.intercept(_super.prototype.delete.call(this, url, options));
    };
    httpInterceptService.prototype.intercept = function (observable) {
        var _this = this;
        return observable.catch(function (err, source) {
            if (err.status == 401 && !err.url.endsWith(err.url, '/token')) {
                _this._router.navigate(['/login']);
                return Observable.empty();
            }
            else {
                return Observable.throw(err);
            }
        });
    };
    return httpInterceptService;
}(Http));
httpInterceptService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [ConnectionBackend,
        RequestOptions,
        Router,
        LocalStorageService])
], httpInterceptService);
export { httpInterceptService };
//# sourceMappingURL=httpIntercept.service.js.map