﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { LocalStorageService } from './localstorage.service';
import { IAuthStatus } from './authStatus';
import { RequestOptionsArgs, RequestOptions, ConnectionBackend, Http, Request, Response, Headers } from "@angular/http";
import { Router } from '@angular/router';

@Injectable()
export class httpInterceptService extends Http {

    headers: Headers = new Headers({ 'Something': 'Something' });
    options1: RequestOptions = new RequestOptions({ headers: this.headers });

    private _authData: any;
    private _authStatus: IAuthStatus;

    constructor(backend: ConnectionBackend,
        defaultOptions: RequestOptions,
        private _router: Router,
        private _localStorageService: LocalStorageService) {
        super(backend, defaultOptions);
    }

    private handleError(error: any) {

        console.error(error);
        let msg: string = '';

        if (error.json().error_description)
            msg = error.json().error_description;

        else {
            let errors = [];
            let modelStates = JSON.parse(error._body).modelState;
            for (let key in modelStates) {

                errors.push(modelStates[key]);
            }

            msg = errors.join(' ');
        }



        return Observable.throw(msg || 'Server error');
    }

    getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }

        // add authorisation header 

        this._authData = {  };
        this._authStatus = this._localStorageService.get('auth');

        if ((!this._authStatus.userName || 0 === this._authStatus.userName.length)) {
            this._authData = this._localStorageService.get('authData');
            options.headers.append('Authorization', 'Bearer ' + this._authData.token);
            options.headers.append('Content-Type', 'application/json');
        }


        
        return options;
    }


    get(url: string, options?: RequestOptionsArgs) {
        
        return this.intercept(super.post(url, this.getRequestOptionArgs(options))).catch(this.handleError);
    }

    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.post(url, body, this.getRequestOptionArgs(options)))
    }

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.put(url, body, this.getRequestOptionArgs(options)));
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.delete(url, options));
    }

    intercept(observable: Observable<Response>): Observable<Response> {
        return observable.catch((err, source) => {
            if (err.status == 401 && ! err.url.endsWith(err.url, '/token')) {
                this._router.navigate(['/login']);
                return Observable.empty();
            } else {
                return Observable.throw(err);
            }
        });

    }

}