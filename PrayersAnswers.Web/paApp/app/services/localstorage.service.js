var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
var LocalStorageService = (function () {
    function LocalStorageService() {
    }
    LocalStorageService.prototype.set = function (name, data) {
        var localData = data;
        localStorage.setItem(name, JSON.stringify(localData));
    };
    LocalStorageService.prototype.get = function (name) {
        var data = JSON.parse(localStorage.getItem(name));
        if (!data) {
            var rawData = localStorage.getItem(name);
            if (!rawData)
                return null;
            else
                data = rawData;
        }
        return data;
    };
    LocalStorageService.prototype.remove = function (name) {
        localStorage.removeItem(name);
    };
    return LocalStorageService;
}());
LocalStorageService = __decorate([
    Injectable()
], LocalStorageService);
export { LocalStorageService };
//# sourceMappingURL=localstorage.service.js.map