﻿import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

    set(name:string, data:any) {
        //let localData = localStorage.getItem('tinyApp');
        //if (localData) {
        //    localData = JSON.parse(localData);
        //} else {
        //    localData = {};
        //}

        let localData = data;

        localStorage.setItem(name, JSON.stringify(localData))
    }

    get(name: string):any {
        
        let data = JSON.parse(localStorage.getItem(name));
        if (!data) {
            let rawData = localStorage.getItem(name);
            if (!rawData)
                return null;
            else
                data = rawData;
        }
        
        
        return data;
    }

    remove(name: string):void
    {
        localStorage.removeItem(name);
    }


}