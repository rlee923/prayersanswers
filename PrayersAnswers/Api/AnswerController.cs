﻿using PrayersAnswers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace PrayersAnswers.Api
{
    [Authorize]
    [RoutePrefix("api/answer")]
    public class AnswerController : ApiController
    {
        IPrayersRepo _repo;

        public AnswerController(IPrayersRepo repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetAnswer(int Id)
        {
            try
            {
                bool flag = !string.IsNullOrEmpty(RequestContext.Principal.Identity.Name);

                if (!flag)
                    return BadRequest();

                var Username = User.Identity.Name;

                var result = await _repo.GetAnswer(Username, Id);

                return Ok(result);

            }

            catch
            {
                return BadRequest();
            }


        }

        [HttpGet]
        //[Route("GetPrayersCount")]
        public async Task<IHttpActionResult> GetAnswersCount()
        {
            try
            {
                bool flag = !string.IsNullOrEmpty(RequestContext.Principal.Identity.Name);



                if (!flag)
                    return BadRequest();
                var Username = User.Identity.Name;
                var result = await _repo.GetAnswersCount(Username);

                return Ok(result);

            }

            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetAnswersByPage/{PageNum?}/{ItemsPerPage?}")]
        public async Task<IHttpActionResult> GetAnswersByPage(int PageNum = 1, int ItemsPerPage = 10)
        {
            try
            {
                bool flag = !string.IsNullOrEmpty(RequestContext.Principal.Identity.Name);



                if (!flag)
                    return BadRequest();
                var Username = User.Identity.Name;
                var result = await _repo.GetAnswersByPage(Username, ItemsPerPage, PageNum);

                return Ok(result);

            }

            catch
            {
                return BadRequest();
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> Post(AnswerVM NewAnswerVM)
        {
            bool flag = (RequestContext.Principal.Identity.Name == NewAnswerVM.UserID);

            if (!flag)
                return BadRequest();

            Answer NewAnswer = new Answer();
            NewAnswer.UserID = NewAnswerVM.UserID;

            NewAnswer.Notes = NewAnswerVM.Notes;

            // check if prayer belongs to the user

            Prayer ThisPrayer = await _repo.GetPrayer(NewAnswerVM.UserID, NewAnswerVM.PrayerID);

            if (ThisPrayer == null)
                return BadRequest();

            // check if this prayer already has an answer

            Answer TestAnswer = await _repo.FindAnswerForPrayer(ThisPrayer.Id);
            if (TestAnswer != null)
                return BadRequest();

            NewAnswer.AnswerFor = ThisPrayer;
            NewAnswer.DateAnswered = DateTime.Now;
            NewAnswer.Type = NewAnswerVM.Type;

            var result = await _repo.AddAnswer(NewAnswer);

            if (result != null)
                return Ok();
            else
                return BadRequest();
        }

        [HttpPut]
        public async Task<IHttpActionResult> Put(AnswerVM CurrentAnswerVM)
        {
            bool flag = !string.IsNullOrEmpty(RequestContext.Principal.Identity.Name);

            if (!flag)
                return BadRequest();

            flag = (RequestContext.Principal.Identity.Name == CurrentAnswerVM.UserID);

            if (!flag)
                return BadRequest();

            string Username = RequestContext.Principal.Identity.Name;

            var result = await _repo.UpdateAnswer(CurrentAnswerVM, Username);

            if (result != null)
                return Ok(result);
            else
                return BadRequest();
        }

        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int AnswerId)
        {
            bool flag = !string.IsNullOrEmpty(RequestContext.Principal.Identity.Name);

            if (!flag)
                return BadRequest();

            string Username = RequestContext.Principal.Identity.Name;

            var result = await _repo.DeleteAnswer(AnswerId, Username);

            if (result > 0)
                return Ok();
            else
                return BadRequest("Not Authorised");
        }
    }
}
