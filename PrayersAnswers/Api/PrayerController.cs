﻿using PrayersAnswers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Routing;

namespace PrayersAnswers.Api
{
    [Authorize]
    [RoutePrefix("api/prayer")]
    //[EnableCors("https://localhost:44390", "*", "*")]
    public class PrayerController : ApiController
    {
        private IPrayersRepo _repo;

        public PrayerController()
        {
            
        }

        public PrayerController(IPrayersRepo repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public async Task<IHttpActionResult> Throw401()
        {
            throw new HttpResponseException(HttpStatusCode.Unauthorized);
        }


        [HttpGet]
        //[Route("GetPrayer/{Id}")]
        public async Task<IHttpActionResult> GetPrayer(int Id)
        {
            try
            {
                bool flag = !string.IsNullOrEmpty(RequestContext.Principal.Identity.Name);

                

                if (!flag)
                    return BadRequest();
                var Username = User.Identity.Name;
                var result = await _repo.GetPrayer(Username, Id);
                
                return Ok(result);

            }

            catch
            {
                return BadRequest();
            }


        }

        [HttpGet]
        //[Route("GetPrayer/{Id}")]
        public async Task<IHttpActionResult> GetPrayerWithAnswer(int Id)
        {
            try
            {
                bool flag = !string.IsNullOrEmpty(RequestContext.Principal.Identity.Name);



                if (!flag)
                    return BadRequest();
                var Username = User.Identity.Name;
                var result = await _repo.GetPrayerWithAnswer(Username, Id);

                return Ok(result);

            }

            catch
            {
                return BadRequest();
            }


        }

        [HttpGet]
        //[Route("GetPrayersCount")]
        public async Task<IHttpActionResult> GetPrayersCount()
        {
            try
            {
                bool flag = !string.IsNullOrEmpty(RequestContext.Principal.Identity.Name);



                if (!flag)
                    return BadRequest();
                var Username = User.Identity.Name;
                var result = await _repo.GetPrayersCount(Username);

                return Ok(result);

            }

            catch
            {
                return BadRequest();
            }
        }


        [HttpGet]
        [Route("GetPrayersByPage/{PageNum?}/{ItemsPerPage?}")]
        public async Task<IHttpActionResult> GetPrayersByPage(int PageNum=1, int ItemsPerPage=10)
        {
            try
            {
                bool flag = !string.IsNullOrEmpty(RequestContext.Principal.Identity.Name);



                if (!flag)
                    return BadRequest();
                var Username = User.Identity.Name;
                var result = await _repo.GetPrayersByPage(Username, ItemsPerPage, PageNum);

                return Ok(result);

            }

            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetPrayersWithAnswersByPage/{PageNum?}/{ItemsPerPage?}")]
        public async Task<IHttpActionResult> GetPrayersWithAnswersByPage(int PageNum = 1, int ItemsPerPage = 10)
        {
            try
            {
                bool flag = !string.IsNullOrEmpty(RequestContext.Principal.Identity.Name);



                if (!flag)
                    return BadRequest();
                var Username = User.Identity.Name;
                var result = await _repo.GetPrayersWithAnswersByPage(Username, ItemsPerPage, PageNum);

                return Ok(result);

            }

            catch
            {
                return BadRequest();
            }
        }




        [HttpPost]
        public async Task<IHttpActionResult> Post(PrayerVM NewPrayerVM)
        {
            bool flag = (RequestContext.Principal.Identity.Name == NewPrayerVM.UserID);

            if (!flag)
                return BadRequest();

            Prayer NewPrayer = new Prayer();
            NewPrayer.UserID = NewPrayerVM.UserID;
            NewPrayer.Title = NewPrayerVM.Title;
            NewPrayer.Notes = NewPrayerVM.Notes;
            NewPrayer.DateStarted = DateTime.Now;

            var result = await _repo.AddPrayer(NewPrayer);

            if (result != null)
                return Ok();
            else
                return BadRequest();
        }

        [HttpPut]
        public async Task<IHttpActionResult> Put(PrayerVM CurrentPrayerVM)
        {
            bool flag = ! string.IsNullOrEmpty(RequestContext.Principal.Identity.Name);

            if (!flag)
                return BadRequest();

            flag = (RequestContext.Principal.Identity.Name == CurrentPrayerVM.UserID);

            if (!flag)
                return BadRequest();

            string Username = RequestContext.Principal.Identity.Name;

            var result = await _repo.UpdatePrayer(CurrentPrayerVM, Username);

            if (result != null)
                return Ok(result);
            else
                return BadRequest();
        }

        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int PrayerId)
        {
            bool flag = !string.IsNullOrEmpty(RequestContext.Principal.Identity.Name);

            if (!flag)
                return BadRequest();

            

            string Username = RequestContext.Principal.Identity.Name;

            var result = await _repo.DeletePrayer(PrayerId, Username);

            if (result > 0)
                return Ok();
            else
                return BadRequest("Not Authorised");
        }
    }
}
