﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using PrayersAnswers.Providers;
using PrayersAnswers.Models;
using PrayersAnswers.Api.Providers;
using Microsoft.Owin.Security.Facebook;
using System.Web.Http;
using Microsoft.Practices.Unity;
using System.Net.Http.Formatting;
using Newtonsoft.Json.Serialization;
using System.Web.Http.Cors;
using System.Web.Http.Routing;
using System.Net.Http;

namespace PrayersAnswers
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }
        public static GoogleOAuth2AuthenticationOptions googleAuthOptions { get; private set; }
        public static FacebookAuthenticationOptions facebookAuthOptions { get; private set; }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {

            
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            //app.UseCookieAuthentication(new CookieAuthenticationOptions());
            //app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);


            HttpConfiguration config = new HttpConfiguration();

            

            var container = new UnityContainer();
            container.RegisterType<IPrayersRepo, PrayersRepo>(new HierarchicalLifetimeManager());
            
            config.DependencyResolver = new UnityResolver(container);

            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
           

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            ConfigureOAuth(app, config);
            

            config.MapHttpAttributeRoutes();

            

            WebApiConfig.Register(config);
            

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //config.EnableCors(new EnableCorsAttribute("*", "*", "*"));
            

            //var constraints = new { httpMethod = new HttpMethodConstraint(HttpMethod.Options) };
            //config.Routes.IgnoreRoute("OPTIONS", "{*pathInfo}", constraints);

            //EnableCorsAttribute cors = new EnableCorsAttribute("*", "*", "GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS");
            //config.EnableCors(cors);


            //app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            //config.EnableCors(new EnableCorsAttribute("*", "*", "*"));
            //app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);


            app.UseWebApi(config);


            



            
        }

        public void ConfigureOAuth(IAppBuilder app, HttpConfiguration config)
        {
            //use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ExternalCookie);
            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();

            

            


            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {

                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(1),
                Provider = new SimpleAuthorizationServerProvider((IPrayersRepo)config.DependencyResolver.GetService(typeof(IPrayersRepo))),
                RefreshTokenProvider = new SimpleRefreshTokenProvider((IPrayersRepo)config.DependencyResolver.GetService(typeof(IPrayersRepo)))
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);

            //app.UseOAuthBearerTokens(OAuthServerOptions);

            //Configure Google External Login
            googleAuthOptions = new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = "136262498807-jvi6ucljudq5o3sri365nb4sk0g38kji.apps.googleusercontent.com",
                ClientSecret = "qCjfmPcag0ISV6-GXfAVNS7K",
                Provider = new GoogleAuthProvider()
            };
            app.UseGoogleAuthentication(googleAuthOptions);

            //Configure Facebook External Login
            facebookAuthOptions = new FacebookAuthenticationOptions()
            {
                AppId = "xxxxxx",
                AppSecret = "xxxxxx",
                Provider = new FacebookAuthProvider()
            };
            app.UseFacebookAuthentication(facebookAuthOptions);

        }
    }
}
