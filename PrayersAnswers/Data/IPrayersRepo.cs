﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PrayersAnswers.Api.Entities;
using PrayersAnswers.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PrayersAnswers.Models
{
    public interface IPrayersRepo
    {

        Task<IdentityResult> RegisterUser(UserModel userModel);
        Task<ApplicationUser> FindUser(string userName, string password);
        Task<bool> AddRefreshToken(RefreshToken token);
        Task<bool> RemoveRefreshToken(string refreshTokenId);
        Task<bool> RemoveRefreshToken(RefreshToken refreshToken);
        Task<RefreshToken> FindRefreshToken(string refreshTokenId);
        List<RefreshToken> GetAllRefreshTokens();
        Task<ApplicationUser> FindAsync(UserLoginInfo loginInfo);
        Task<IdentityResult> CreateAsync(ApplicationUser user);
        Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login);
        Client FindClient(string clientId);

        Task<Prayer> AddPrayer(Prayer NewPrayer);
        Task<int> GetPrayersCount(string Username);
        Task<List<Prayer>> GetPrayers(string Username);
        Task<Prayer> GetPrayer(string Username, int PrayerID);
        Task<PrayerWithAnswer> GetPrayerWithAnswer(string Username, int PrayerID);
        Task<List<Prayer>> GetPrayersByPage(string Username, int ItemsPerPage = 10, int PageNum = 1);
        Task<List<PrayerWithAnswer>> GetPrayersWithAnswersByPage(string Username, int ItemsPerPage = 10, int PageNum = 1);
        Task<Prayer> UpdatePrayer(PrayerVM CurrentPrayer, string Username);
        Task<int> DeletePrayer(int PrayerId, string Username);

        Task<Answer> AddAnswer(Answer NewAnswer);
        Task<int> GetAnswersCount(string Username);
        Task<List<Answer>> GetAnswers(string Username);            
        Task<Answer> GetAnswer(string Username, int AnswerID);
        Task<List<Answer>> GetAnswersByPage(string Username, int ItemsPerPage = 10, int PageNum = 1);
        Task<Answer> UpdateAnswer(AnswerVM CurrentAnswerVM, string Username);
        Task<int> DeleteAnswer(int PrayerId, string Username);
        Task<Answer> FindAnswerForPrayer(int PrayerID);

    }
}