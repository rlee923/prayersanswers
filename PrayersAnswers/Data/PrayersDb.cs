﻿using PrayersAnswers.Api.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PrayersAnswers.Models
{
    public class PrayersDb : ApplicationDbContext
    {
        public PrayersDb(): base()
        {

        }

        public DbSet<Prayer> Prayers { get; set; }
        public DbSet<Answer> Answers { get; set; }

        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
    }
}