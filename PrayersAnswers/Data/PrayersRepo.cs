﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PrayersAnswers.Api.Entities;
using PrayersAnswers.Api.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PrayersAnswers.Models
{
    public class PrayersRepo : IPrayersRepo, IDisposable
    {
        private PrayersDb _db;

        private UserManager<ApplicationUser> _userManager;

        public PrayersRepo(PrayersDb db)
        {
            _db = db;
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_db));
        }

        #region authentication


        public async Task<IdentityResult> RegisterUser(UserModel userModel)
        {
            ApplicationUser user = new ApplicationUser
            {
                UserName = userModel.UserName,
                Email = userModel.EmailAddress,                
                EmailConfirmed = true
            };

            if(userModel.Password!= userModel.ConfirmPassword)
            {
                return IdentityResult.Failed();
            }

            
                

            var result = await _userManager.CreateAsync(user, userModel.Password);

            return result;
        }

        public async Task<ApplicationUser> FindUser(string userName, string password)
        {
            ApplicationUser user = await _userManager.FindAsync(userName, password);
            if (user == null)
            { 
                user = await _userManager.FindByEmailAsync(userName);
                if( user!=null)
                {
                    if (!await _userManager.CheckPasswordAsync(user, password))
                    {
                        user = null;
                    }
                }
            }


            return user;
        }

        public Client FindClient(string clientId)
        {
            var client = _db.Clients.Find(clientId);

            return client;
        }

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {

            var existingToken = _db.RefreshTokens.Where(r => r.Subject == token.Subject && r.ClientId == token.ClientId).SingleOrDefault();

            if (existingToken != null)
            {
                var result = await RemoveRefreshToken(existingToken);
            }

            _db.RefreshTokens.Add(token);

            return await _db.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
            var refreshToken = await _db.RefreshTokens.FindAsync(refreshTokenId);

            if (refreshToken != null)
            {
                _db.RefreshTokens.Remove(refreshToken);
                return await _db.SaveChangesAsync() > 0;
            }

            return false;
        }

        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            _db.RefreshTokens.Remove(refreshToken);
            return await _db.SaveChangesAsync() > 0;
        }

        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = await _db.RefreshTokens.FindAsync(refreshTokenId);

            return refreshToken;
        }

        public List<RefreshToken> GetAllRefreshTokens()
        {
            return _db.RefreshTokens.ToList();
        }

        public async Task<ApplicationUser> FindAsync(UserLoginInfo loginInfo)
        {
            ApplicationUser user = await _userManager.FindAsync(loginInfo);

            return user;
        }

        public async Task<IdentityResult> CreateAsync(ApplicationUser user)
        {
            var result = await _userManager.CreateAsync(user);

            return result;
        }

        public async Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            var result = await _userManager.AddLoginAsync(userId, login);

            return result;
        }

        #endregion authentication


        #region prayers


        public async Task<Prayer> AddPrayer(Prayer NewPrayer)
        {
            _db.Prayers.Add(NewPrayer);
            
            try
            {
                int result = await _db.SaveChangesAsync();
                return NewPrayer;
            }
            catch
            {
                return null;
            }


        }

        public async Task<int> GetPrayersCount(string Username)
        {
            var result = await _db.Prayers.Where(x => x.UserID == Username).ToListAsync();
            return result.Count;
        }


        public async Task<List<Prayer>> GetPrayers(string Username)
        {
            var result = await _db.Prayers.Where(x => x.UserID == Username).ToListAsync();
            return result;

        }



        public async Task<Prayer> GetPrayer(string Username, int PrayerID)
        {
            var result = await _db.Prayers
                .FirstOrDefaultAsync(x => x.UserID == Username && x.Id == PrayerID);
            return result;

        }

        public async Task<PrayerWithAnswer> GetPrayerWithAnswer(string Username, int PrayerID)
        {
            var result = await _db.Prayers
                .FirstOrDefaultAsync(x => x.UserID == Username && x.Id == PrayerID);
            var CurrentAnswer = await _db.Answers
                .Where(x => x.AnswerFor != null)
                .SingleOrDefaultAsync(x => x.AnswerFor.Id == PrayerID);
            return new PrayerWithAnswer { Prayer = result, Answer = CurrentAnswer };

        }

        public async Task<List<Prayer>> GetPrayersByPage(string Username, int ItemsPerPage = 10, int PageNum = 1)
        {
            try
            {


                var result = await _db.Prayers.Where(x => x.UserID == Username)
                            .OrderBy(x=> x.Id)
                            .Skip((PageNum - 1) * ItemsPerPage)
                            .Take(ItemsPerPage)
                            .ToListAsync();
                return result;
            }
            catch (Exception e)
            {
                return null;
            }

        }


        public async Task<List<PrayerWithAnswer>> GetPrayersWithAnswersByPage(string Username, int ItemsPerPage = 10, int PageNum = 1)
        {
            try
            {


                //var result = await _db.Prayers.Where(x => x.UserID == Username)
                //            .OrderBy(x => x.Id)
                //            .Skip((PageNum - 1) * ItemsPerPage)
                //            .Take(ItemsPerPage)
                //            .ToListAsync();
                var result = await
                            (from prayers in _db.Prayers.Where(x=> x.UserID==Username)
                             join answers in _db.Answers.Where(x=> x.UserID==Username) on prayers.Id equals answers.AnswerFor.Id into pa
                             from answers2 in pa.DefaultIfEmpty()
                             


                             select new PrayerWithAnswer { Prayer =  prayers,  Answer = answers2 })
                             .OrderBy(x => x.Prayer.Id)
                            .Skip((PageNum - 1) * ItemsPerPage)
                            .Take(ItemsPerPage)
                            .ToListAsync();
                

                return result;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        

        public async Task<Prayer> UpdatePrayer(PrayerVM CurrentPrayer, string Username)
        {
            if (CurrentPrayer.UserID != Username)
                return null;

            var RecordToChange = await _db.Prayers.FirstOrDefaultAsync(x => x.Id == CurrentPrayer.Id && x.UserID==CurrentPrayer.UserID);
            if (RecordToChange != null)
            {
                RecordToChange.Notes = CurrentPrayer.Notes;
                RecordToChange.Title = CurrentPrayer.Title;

            }

            _db.Entry(RecordToChange).State = EntityState.Modified;
            try
            {
                var result = await _db.SaveChangesAsync();

                if (result == 0)
                {
                    throw new Exception("Failed to save modified prayer");
                }

                return RecordToChange;
            }
            catch
            {
                return null;
            }

        }


        public async Task<int> DeletePrayer(int PrayerId, string Username)
        {

            try
            {
                var AnswerToDelete = _db.Answers.FirstOrDefault(x => x.AnswerFor.Id == PrayerId);

                if(AnswerToDelete!=null)
                    _db.Answers.Remove(AnswerToDelete);

                var ItemToDelete = _db.Prayers.FirstOrDefault(x => x.Id == PrayerId && x.UserID==Username);

                if (ItemToDelete != null)
                {
                    _db.Prayers.Remove(ItemToDelete);
                    await _db.SaveChangesAsync();
                    return 1;
                }
                else
                    return 0;
                
                
            }


            catch (Exception)
            {
                return 0;
            }


        }


        #endregion prayers


        #region answers


        public async Task<List<Answer>> GetAnswers(string Username)
        {
            var result = await _db.Answers.Where(x => x.UserID == Username).ToListAsync();
            return result;

        }

        public async Task<Answer> GetAnswer(string Username,int Id)
        {
            var result = await _db.Answers.SingleOrDefaultAsync(x => x.Id == Id && x.UserID == Username);
            return result;

        }

        public async Task<int> GetAnswersCount(string Username)
        {
            var result = await _db.Answers.Where(x => x.UserID == Username).ToListAsync();
            return result.Count();
        }

        public async Task<Answer> AddAnswer(Answer NewAnswer)
        {
            _db.Answers.Add(NewAnswer);

            try
            {
                int result = await _db.SaveChangesAsync();
                return NewAnswer;
            }
            catch
            {
                return null;
            }


        }

        public async Task<List<Answer>> GetAnswersByPage(string Username, int ItemsPerPage = 10, int PageNum = 1)
        {
            var result = await _db.Answers.Where(x => x.UserID == Username)
                        .OrderByDescending(x=> x.DateAnswered)
                        .Skip((PageNum - 1) * ItemsPerPage)
                        .Take(ItemsPerPage)
                        .ToListAsync();
            return result;

        }

        public async Task<Answer> UpdateAnswer(AnswerVM CurrentAnswer, string Username)
        {
            if (CurrentAnswer.UserID != Username)
                return null;

            var RecordToChange = await _db.Answers.FirstOrDefaultAsync(x => x.Id == CurrentAnswer.Id && x.UserID == CurrentAnswer.UserID);
            if (RecordToChange != null)
            {
                RecordToChange.Notes = CurrentAnswer.Notes;
                RecordToChange.Type = CurrentAnswer.Type;
                
            }

            _db.Entry(RecordToChange).State = EntityState.Modified;
            try
            {
                var result = await _db.SaveChangesAsync();

                if (result == 0)
                {
                    throw new Exception("Failed to save modified answer");
                }

                return RecordToChange;
            }
            catch
            {
                return null;
            }

        }


        public async Task<int> DeleteAnswer(int AnswerId, string Username)
        {

            try
            {
                var ItemToDelete = _db.Answers.FirstOrDefault(x => x.Id == AnswerId && x.UserID == Username);

                if (ItemToDelete != null)
                {
                    _db.Answers.Remove(ItemToDelete);
                    await _db.SaveChangesAsync();
                    return 1;
                }
                else
                    return 0;


            }


            catch (Exception)
            {
                return 0;
            }


        }

        public async Task<Answer> FindAnswerForPrayer(int PrayerID)
        {
            Answer CurrentAnswer = await _db.Answers
                .Where(x => x.AnswerFor != null)
                .SingleOrDefaultAsync(x=> x.AnswerFor.Id == PrayerID);
            return CurrentAnswer;
        }

        #endregion answers


        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if(!this.disposed)
            {
                if(disposing)
                {
                    _db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);

        }
    }
}