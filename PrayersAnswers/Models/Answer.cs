﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PrayersAnswers.Models
{
    public class Answer
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public string UserID { get; set; }
        [Required]
        public DateTime DateAnswered { get; set; }
        [Required]
        public AnswerType Type { get; set; }
        public string Notes { get; set; }
        public virtual Prayer AnswerFor { get; set; }
    }

    public enum AnswerType
    {
        OK=1,
        Wait=2,
        No=3

    }
}