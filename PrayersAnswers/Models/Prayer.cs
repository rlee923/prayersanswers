﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PrayersAnswers.Models
{
    public class Prayer
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public string UserID { get; set; }
        [Required]
        public DateTime DateStarted { get; set; }

        public string Notes { get; set; }
        [MaxLength(150)]
        public string Title { get; set; }
    }


    
}