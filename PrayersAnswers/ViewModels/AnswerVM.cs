﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrayersAnswers.Models
{
    public class AnswerVM
    {
        public int Id { get; set; }
        public string UserID { get; set; }        
        public string Notes { get; set; }
        public int PrayerID { get; set; }
        public AnswerType Type { get; set; }
    }
}