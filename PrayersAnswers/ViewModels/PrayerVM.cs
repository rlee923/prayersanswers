﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrayersAnswers.Models
{
    public class PrayerVM
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public string Title { get; set; }
        public string Notes { get; set; }
    }
}