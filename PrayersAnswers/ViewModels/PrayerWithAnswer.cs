﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrayersAnswers.Models
{
    public class PrayerWithAnswer
    {
        public Prayer Prayer;
        public Answer Answer;

        public PrayerWithAnswer()
        {
            
        }


        public PrayerWithAnswer(Prayer NewPrayer, Answer NewAnswer)
        {
            Prayer = NewPrayer;
            Answer = NewAnswer;
        }

    }
}